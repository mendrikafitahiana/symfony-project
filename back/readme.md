# Back

 ## Prerequisites

    download symfony cli : https://symfony.com/download

    symfony console doctrine:migrations:migrate

    execute the queries written in the Base.sql file in MySQL

## installation

- composer update

- gerenate the ssl keys : cd back/config  mkdir jwt
    - private key : 1) execute this command : openssl genrsa -out config/jwt/private.pem -aes256 4096
                    2) a password will be requested, put this in .env file (JWT_PASSPHRASE=your password)

    - public key : 1) execute this command : openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem
                    2) the same password 

## run 
 symfony server:start



