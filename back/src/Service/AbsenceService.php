<?php 

namespace App\Service;

use App\Repository\AbsenceRepository;
use App\Entity\Absence;
use Doctrine\Persistence\ManagerRegistry;

class AbsenceService 
{
    private $absrepo;
    private $manager;

    public function __construct(AbsenceRepository $repoabs, ManagerRegistry $man) 
    {
        $this->absrepo = $repoabs;
        $this->manager = $man;
    }

    public function enregisterAbs(Absence $abs)
    {
        $succes = "";
        $echec = "";
        $dateabs = $abs->getDateabsence()->format('Y-m-d');
        $nb = $this->getAbsenceDejaEnregistree($dateabs, $abs->getIdemploye());
        $conge = $this->getDateCongeAbs($dateabs, $abs->getIdemploye());

        try {
            if($nb > 0) {
                $echec = "Cette absence a déja été enregistrée";
            }
            elseif($conge > 0) {
                $echec = "Cette absence fait partie de ces congés";
            }
            else {
                $this->absrepo->add($abs, true);
                $succes = "Votre enregistrement a été un succès";
            }
        }
        catch(Exception $e) {
            $echec = "Erreur ! Votre enregistrement a été un echec";
        }

        $result = array("succes" => $succes, "erreur" => $echec);

        return $result;
        
    }

    public function getAbsenceDejaEnregistree($dateabs, $idemp) {
        $sql = "select count(id) nombre from absence where dateabsence = :dateabsence and idemploye = :idemp";

        $conn = $this->manager->getManager()->getConnection();
        $stmt = $conn->prepare($sql);

        // $res = $stmt->execute(array('dateabsence' => $dateabs, 'idemp' => $idemp));
        $stmt->execute(array('dateabsence' => $dateabs, 'idemp' => $idemp));


        // $aa = $res->fetchAssociative();
        $aa = $stmt->fetchAssociative();

        return $aa['nombre'];
    }

    public function getDateCongeAbs($dateabs, $idemp) {
        $sql = "select count(id) nombre from listeconge where etat_validation = 3 and idposte = 3 and idemploye = :idemp and (date_debut = :dateabs or date_fin = :dateabs)";

        $conn = $this->manager->getManager()->getConnection();
        $stmt = $conn->prepare($sql);

        // $res = $stmt->execute(array('idemp' => $idemp, 'dateabs' => $dateabs));
        $stmt->execute(array('idemp' => $idemp, 'dateabs' => $dateabs));


        // $aa = $res->fetchAssociative();
        $aa = $stmt->fetchAssociative();

        return $aa['nombre'];
    }

    public function getHistoriqueAbs($numero, $idemp) {
        $sql = "";
        if($idemp == 0) {
            if($numero == 1) {
                $sql = "select * from listeabsence where dateabsence >= (now() - interval 7 day) order by dateabsence desc";
            }
            if($numero == 2) {
                $sql = "select * from listeabsence where dateabsence >= (now() - interval 3 month) order by dateabsence desc";
            }
            if($numero == 3) {
                $sql = "select * from listeabsence where dateabsence >= (now() - interval 6 month) order by dateabsence desc";
            }
            else {
                $sql = "select * from listeabsence where dateabsence >= (now() - interval 1 year) order by dateabsence desc";
            }

            $conn = $this->manager->getManager()->getConnection();
            $stmt = $conn->prepare($sql);
            // $res = $stmt->execute();
            $stmt->execute();
        }
        else {
            if($numero == 1) {
                $sql = "select * from listeabsence where dateabsence >= (now() - interval 7 day) and id = :idempl order by dateabsence desc";
            }
            if($numero == 2) {
                $sql = "select * from listeabsence where dateabsence >= (now() - interval 3 month) and id = :idempl order by dateabsence desc";
            }
            if($numero == 3) {
                $sql = "select * from listeabsence where dateabsence >= (now() - interval 6 month) and id = :idempl order by dateabsence desc";
            }
            else {
                $sql = "select * from listeabsence where dateabsence >= (now() - interval 1 year) and id = :idempl order by dateabsence desc";
            }

            $conn = $this->manager->getManager()->getConnection();
            $stmt = $conn->prepare($sql);
            // $res = $stmt->execute(array("idempl" => $idemp));
            $stmt->execute(array("idempl" => $idemp));
        }

        // return $res->fetchAllAssociative();
        return $stmt->fetchAllAssociative();
    }

    public function statAbsence() {
        $sql = "select 
        (case
            when m.mois = 1 then 'Janvier'
            when m.mois = 2 then 'Février'
            when m.mois = 3 then 'Mars'
            when m.mois = 4 then 'Avril'
            when m.mois = 5 then 'Mai'
            when m.mois = 6 then 'Juin'
            when m.mois = 7 then 'Juillet'
            when m.mois = 8 then 'Août'
            when m.mois = 9 then 'Septembre'
            when m.mois = 10 then 'Octobre'
            when m.mois = 11 then 'Novembre'
            else 'Décembre'
        end) mois,
        (case when s.nbabsents is null then 0 else s.nbabsents end) nbabsents
        from 
        (
            SELECT 1 AS mois
            UNION SELECT 2 AS mois
            UNION SELECT 3 AS mois
            UNION SELECT 4 AS mois
            UNION SELECT 5 AS mois
            UNION SELECT 6 AS mois
            UNION SELECT 7 AS mois
            UNION SELECT 8 AS mois
            UNION SELECT 9 AS mois
            UNION SELECT 10 AS mois
            UNION SELECT 11 AS mois
            UNION SELECT 12 AS mois
        ) as m
        left join statabs s on s.mois = m.mois
        group by m.mois,s.nbabsents";

        $conn = $this->manager->getManager()->getConnection();
        $stmt = $conn->prepare($sql);
        // $res = $stmt->execute();
        $stmt->execute();
        // $statabs = $res->fetchAllAssociative();
        $statabs = $stmt->fetchAllAssociative();

        return $statabs;
    }
}
?>