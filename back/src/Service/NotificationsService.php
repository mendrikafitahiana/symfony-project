<?php 

namespace App\Service;

use Doctrine\Persistence\ManagerRegistry;

class NotificationsService 
{
    private $manager;

    public function __construct(ManagerRegistry $man) 
    {
        $this->manager = $man;
    }

    public function nombreNotif($idempconcerner) {
        $sql = "select count(id) nbnotif from notifications where concerner = :idempc and vue = false";

        $conn = $this->manager->getManager()->getConnection();
        $stmt = $conn->prepare($sql);

        // $res = $stmt->execute(array('idempc' => $idempconcerner));
        $stmt->execute(array('idempc' => $idempconcerner));
        // $result = $res->fetchAssociative();
        $result = $stmt->fetchAssociative();

        return $result['nbnotif'];
    }

    public function getNotifs($idempconcerner) {
        $sql = "select * from notifications where concerner = :idempc and date(createdat) >= (now() - interval 3 month) order by createdat desc";

        $conn = $this->manager->getManager()->getConnection();
        $stmt = $conn->prepare($sql);

        // $res = $stmt->execute(array('idempc' => $idempconcerner));
        $stmt->execute(array('idempc' => $idempconcerner));

        // $result = $res->fetchAllAssociative();
        $result = $stmt->fetchAllAssociative();


        return $result;
    }

    public function setVuNotif($id) {
        $sql = "update notifications set vue = true,vu_le = current_timestamp where id = :idnotif";

        $conn = $this->manager->getManager()->getConnection();
        $stmt = $conn->prepare($sql);

        $stmt->execute(array('idnotif' => $id));
    }
}

?>