<?php

namespace App\Service;

use Doctrine\Persistence\ManagerRegistry;

class ListeCongeService
{
    private $manager;

    public function __construct(ManagerRegistry $manage) 
    {
        $this->manager = $manage;
    }

    public function getListeCongeGlobal($idemp, $statut, $numero) 
    {
        $sql = ""; 

        if($statut == 0) {
            if($numero == 1) {
                $sql = "select id, date_debut, date_fin, natureconge, nombrejours, createdat, statut from listeconge where idposte = 3 and idemploye = :idempl and date(createdat) >= (now() - interval 7 day) order by createdat desc";
            }
            elseif($numero == 2) {
                $sql = "select id, date_debut, date_fin, natureconge, nombrejours, createdat, statut from listeconge where idposte = 3 and idemploye = :idempl and date(createdat) >= (now() - interval 3 month) order by createdat desc";
            }
            elseif($numero == 3) {
                $sql = "select id, date_debut, date_fin, natureconge, nombrejours, createdat, statut from listeconge where idposte = 3 and idemploye = :idempl and date(createdat) >= (now() - interval 6 month) order by createdat desc";
            }
            else {
                $sql = "select id, date_debut, date_fin, natureconge, nombrejours, createdat, statut from listeconge where idposte = 3 and idemploye = :idempl and date(createdat) >= (now() - interval 1 year) order by createdat desc";
            }

            $conn = $this->manager->getManager()->getConnection();
            $stmt = $conn->prepare($sql);
    
            // $res = $stmt->execute(array('idempl' => $idemp));
            $stmt->execute(array('idempl' => $idemp));

        }
        else {
            if($numero == 1) {
                $sql = "select id, date_debut, date_fin, natureconge, nombrejours, createdat, statut from listeconge where idposte = 3 and etat_validation = :stat and idemploye = :idempl and date(createdat) >= (now() - interval 7 day) order by createdat desc";
            }
            elseif($numero == 2) {
                $sql = "select id, date_debut, date_fin, natureconge, nombrejours, createdat, statut from listeconge where idposte = 3 and etat_validation = :stat and idemploye = :idempl and date(createdat) >= (now() - interval 3 month) order by createdat desc";
            }
            elseif($numero == 3) {
                $sql = "select id, date_debut, date_fin, natureconge, nombrejours, createdat, statut from listeconge where idposte = 3 and etat_validation = :stat and idemploye = :idempl and date(createdat) >= (now() - interval 6 month) order by createdat desc";
            }
            else {
                $sql = "select id, date_debut, date_fin, natureconge, nombrejours, createdat, statut from listeconge where idposte = 3 and etat_validation = :stat and idemploye = :idempl and date(createdat) >= (now() - interval 1 year) order by createdat desc";
            }
            
            $conn = $this->manager->getManager()->getConnection();
            $stmt = $conn->prepare($sql);
    
            // $res = $stmt->execute(array('stat' => $statut, 'idempl' => $idemp));
            $stmt->execute(array('stat' => $statut, 'idempl' => $idemp));
        }
       

        // return $res->fetchAllAssociative();
        return $stmt->fetchAllAssociative();
    }

    public function getDetailConge($idconge) 
    {
        $sql = "select * from listeconge where id = :idconge";
        $conn = $this->manager->getManager()->getConnection();
        $stmt = $conn->prepare($sql);
        // $res = $stmt->execute(array('idconge' => $idconge));
        $stmt->execute(array('idconge' => $idconge));
        
        // return $res->fetchAllAssociative();
        return $stmt->fetchAllAssociative();
    }

    public function getHistoriqueConge($numero,$idemp) 
    {
        $sql = "";
        if ($idemp == 0) {
            if ($numero == 1) {
                $sql = "select * from listeconge where date(createdat) >= (now() - interval 7 day) and idposte = 3 and etat_validation > 1 order by createdat desc";
            }
            elseif ($numero == 2) {
                $sql = "select * from listeconge where date(createdat) >= (now() - interval 3 month) and idposte = 3 and etat_validation > 1 order by createdat desc";
            }
            elseif ($numero == 3) {
                $sql = "select * from listeconge where date(createdat) >= (now() - interval 6 month) and idposte = 3 and etat_validation > 1 order by createdat desc";
            }
            else {
                $sql = "select * from listeconge where date(createdat) >= (now() - interval 1 year) and idposte = 3 and etat_validation > 1 order by createdat desc";
            }

            $conn = $this->manager->getManager()->getConnection();
            $stmt = $conn->prepare($sql);
            // $res = $stmt->execute();
            $stmt->execute();
        }
        else {
            if ($numero == 1) {
                $sql = "select * from listeconge where date(createdat) >= (now() - interval 7 day) and idposte = 3 and etat_validation > 1 and idemploye = :idemp order by createdat desc";
            }
            elseif ($numero == 2) {
                $sql = "select * from listeconge where date(createdat) >= (now() - interval 3 month) and idposte = 3 and etat_validation > 1 and idemploye = :idemp order by createdat desc";
            }
            elseif ($numero == 3) {
                $sql = "select * from listeconge where date(createdat) >= (now() - interval 6 month) and idposte = 3 and etat_validation > 1 and idemploye = :idemp order by createdat desc";
            }
            else {
                $sql = "select * from listeconge where date(createdat) >= (now() - interval 1 year) and idposte = 3 and etat_validation > 1 and idemploye = :idemp order by createdat desc";
            }

            $conn = $this->manager->getManager()->getConnection();
            $stmt = $conn->prepare($sql);
            // $res = $stmt->execute(array("idemp" => $idemp));
            $stmt->execute(array("idemp" => $idemp));
        }

        // return $res->fetchAllAssociative();
        return $stmt->fetchAllAssociative();
    }

    public function getCalendrierConge($idempl) {
        $sql = "select date_debut, date_fin from listeconge where etat_validation = 3 and idemploye = :idempl and idposte = 3";

        $conn = $this->manager->getManager()->getConnection();
        $stmt = $conn->prepare($sql);
        // $res = $stmt->execute(array("idempl" => $idempl));
        $stmt->execute(array("idempl" => $idempl));


        // return $res->fetchAllAssociative();
        return $stmt->fetchAllAssociative();
    }

    public function getCalendrierAbsence($idempl) {
        $sql = "select dateabsence from absence where idemploye = :idempl ";

        $conn = $this->manager->getManager()->getConnection();
        $stmt = $conn->prepare($sql);
        // $res = $stmt->execute(array("idempl" => $idempl));
        $stmt->execute(array("idempl" => $idempl));

        // return $res->fetchAllAssociative();
        return $stmt->fetchAllAssociative();
    }

    public function getToutCalendrier($idempl) {
        $conge = $this->getCalendrierConge($idempl);
        $absence = $this->getCalendrierAbsence($idempl);

        $tout = array();
        $result = array();

        for($i=0; $i<count($conge); $i++) {
            $tout = array("date_debut" => $conge[$i]['date_debut'], "date_fin" => $conge[$i]['date_fin'], "type" => "Congés", "color" => "green");
            array_push($result, $tout);
        }
        for($j=0; $j<count($absence); $j++) {
            $tout = array("date_debut" => $absence[$j]['dateabsence'], "date_fin" => $absence[$j]['dateabsence'], "type" => "Absence", "color" => "red");
            array_push($result, $tout);
        }

        return $result;
    }

    public function tableauDeBord() {
        $result = array();
        $conn = $this->manager->getManager()->getConnection();

        $sql1 = "select employe.id, employe.nom, employe.prenom, employe.solde_conge, employe.solde_conge + 2.5 solde_conge_prochain, 
        (case when tb.totalconge is null then 0.00 else tb.totalconge end) totalconge
        from tb 
        right join employe on employe.id = tb.id order by employe.nom";
        $stmt = $conn->prepare($sql1);
        // $res = $stmt->execute();
        $stmt->execute();
        // $conge = $res->fetchAllAssociative();
        $conge = $stmt->fetchAllAssociative();

        $sql2 = "select employe.id, employe.nom, employe.prenom, employe.solde_conge, employe.solde_conge + 2.5 solde_conge_prochain, 
        (case when tba.totalabsence is null then 0.00 else tba.totalabsence end) totalabsence
        from tba 
        right join employe on employe.id = tba.id order by employe.nom";
        $stmt = $conn->prepare($sql2);
        // $res = $stmt->execute();
        $stmt->execute();
        // $absence = $res->fetchAllAssociative();
        $absence = $stmt->fetchAllAssociative();

        for($i = 0; $i<count($conge); $i++) {
            $val = array("id" => $conge[$i]['id'], "nom" => $conge[$i]['nom'], "prenom" => $conge[$i]['prenom'], "soldeconge" => $conge[$i]['solde_conge'], "soldecongeprochain" => $conge[$i]["solde_conge_prochain"], "totalconge" => $conge[$i]['totalconge'], "totalabsence" => $absence[$i]['totalabsence']);
            array_push($result, $val);
        }

        return $result;
    }

    public function getNow() {
        $mois = date("m");
        $annee = date("Y");
        $nbjours = date("t");

        $monthname = "";

        switch ($mois) {
            case "01": 
                $monthname = "Janvier";
                break;
            case "02":
                $monthname = "Février";
                break;
            case "03":
                $monthname = "Mars";
                break;
            case "04":
                $monthname = "Avril";
                break;
            case "05":
                $monthname = "Mai";
                break;
            case "06":
                $monthname = "Juin";
                break;
            case "07":
                $monthname = "Juillet";
                break;
            case "08":
                $monthname = "Août";
                break;
            case "09":
                $monthname = "Septembre";
                break;
            case "10":
                $monthname = "Octobre";
                break;
            case "11":
                $monthname = "Novembre";
                break;
            case "12":
                $monthname = "Décembre";
                break;
        }

        $now = $monthname." ".$annee;
        return array("now" => $now, "nbjours" => $nbjours);
    } 

    public function statConge() {
        $sql = "select 
        (case  
            when m.mois = 1 then 'Janvier'
            when m.mois = 2 then 'Février'
            when m.mois = 3 then 'Mars'
            when m.mois = 4 then 'Avril'
            when m.mois = 5 then 'Mai'
            when m.mois = 6 then 'Juin'
            when m.mois = 7 then 'Juillet'
            when m.mois = 8 then 'Août'
            when m.mois = 9 then 'Septembre'
            when m.mois = 10 then 'Octobre'
            when m.mois = 11 then 'Novembre'
            else 'Décembre'
        end) mois, 
        (case when s.nbconge is null then 0 else s.nbconge end) nbconge
        from
        (
            SELECT 1 AS mois
            UNION SELECT 2 AS mois
            UNION SELECT 3 AS mois
            UNION SELECT 4 AS mois
            UNION SELECT 5 AS mois
            UNION SELECT 6 AS mois
            UNION SELECT 7 AS mois
            UNION SELECT 8 AS mois
            UNION SELECT 9 AS mois
            UNION SELECT 10 AS mois
            UNION SELECT 11 AS mois
            UNION SELECT 12 AS mois
        ) as m
        left join stat s on s.mois = m.mois
        group by m.mois,s.nbconge";

        $conn = $this->manager->getManager()->getConnection();
        $stmt = $conn->prepare($sql);
        // $res = $stmt->execute();
        $stmt->execute();
        // $statconge = $res->fetchAllAssociative();
        $statconge = $stmt->fetchAllAssociative();

        return $statconge;
    }
}