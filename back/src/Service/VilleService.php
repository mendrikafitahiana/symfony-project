<?php

namespace App\Service;

use App\Repository\VilleRepository;
use Doctrine\Persistence\ManagerRegistry;

class VilleService
{
    private $villerepository;
    private $manager;

    public function __construct(VilleRepository $ville, ManagerRegistry $manage)
    {
        $this->villerepository = $ville;
        $this->manager = $manage;
    }

    public function findVille(): array{
        $villes = $this->villerepository->findAll();

        return $villes;
    }
}