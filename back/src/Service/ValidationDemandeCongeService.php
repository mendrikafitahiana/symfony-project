<?php 

namespace App\Service;

use Doctrine\Persistence\ManagerRegistry;
use App\Service\EmployeService;
use App\Entity\Notifications;

class ValidationDemandeCongeService
{
    private $manager;
    private $mailer;
    private $empserv; 
    public function __construct(ManagerRegistry $man, \Swift_Mailer $mail, EmployeService $empservc) 
    {
        $this->manager = $man;
        $this->mailer = $mail;
        $this->empserv = $empservc;
    }

    public function getDemandeByEmpValidateur($idempvalidateur) 
    {
        $sql = "select * from listeconge where idemployevalidateur = :idempval and etat_validation = 2 or etat_validation = 5 order by createdat desc";

        $conn = $this->manager->getManager()->getConnection();
        $stmt = $conn->prepare($sql);
        // $res = $stmt->execute(array('idempval' => $idempvalidateur));
        $stmt->execute(array('idempval' => $idempvalidateur));


        // return $res->fetchAllAssociative();
        return $stmt->fetchAllAssociative();

    }

    public function validationDemandeConge($etat, $iddemande, $idempvalidateur, $nomempv, $prenomempv, $datedebut, $datefin, $email, $emailempv, $idempd, $nbjours, $coms) 
    {
        $sql = "update validation_demande_user set etat = :etat, commentaires_validation = :coms where iddemandeconge = :iddemande and idemployevalidateur = :idempv";

        $conn = $this->manager->getManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute(array('etat' => $etat, "coms" => $coms, "iddemande" => $iddemande, "idempv" => $idempvalidateur));

        $datenow = date("Y-m-d");

        if($etat == 3) {
            
            // Insert notifications
                $notifs = new Notifications();
                $notifs->setCreateur($idempvalidateur);
                $notifs->setConcerner($idempd);
                $notifs->setVue(false);
                $notifs->setDateDebut(new \DateTime($datedebut));
                $notifs->setDateFin(new \DateTime($datefin));
                $notifs->setIddemande($iddemande);
                $notifs->setNotif(
                    "Votre demande a été validée par ".$nomempv." ".$prenomempv." le ".$datenow.""
                );
                $notifs->setType(1);

                $entity3 = $this->manager->getManager();
                $entity3->persist($notifs);
                $entity3->flush();
            // 

            $empdemandeur = $this->empserv->getEMpById($idempd);
            $nvsolde = floatval($empdemandeur->getSoldeConge()) - floatval($nbjours);

            $empval = $this->empserv->getEMpById($idempvalidateur);

            if($empval->getIdposte() == 3) {
                $empdemandeur->setSoldeConge($nvsolde);
                $entity = $this->manager->getManager();
                $entity->flush();
            }

            $envoiemail = new \Swift_Message('Validation de la demande de congé du '.strval($datedebut).' au '.strval($datefin));
            $envoiemail->setFrom($emailempv);
            $envoiemail->setTo($email);
            $envoiemail->setBody(
                '<p>Bonjour,</p>
                
                Votre demande a été validée par '.$nomempv.''.$prenomempv.' le '.strval($datenow).'
                ','text/html'
            );
            $this->mailer->send($envoiemail);
        }
        if($etat == 5) {
            // Insert notifications
                $notifs = new Notifications();
                $notifs->setCreateur($idempvalidateur);
                $notifs->setConcerner($idempd);
                $notifs->setVue(false);
                $notifs->setDateDebut(new \DateTime($datedebut));
                $notifs->setDateFin(new \DateTime($datefin));
                $notifs->setIddemande($iddemande);
                $notifs->setNotif(
                    "Votre demande a été mise en réserve par ".$nomempv." ".$prenomempv." le ".$datenow.".
                    Cette demande vous sera consulter afin d'avoir une bonne entente de la validation de celle-ci."
                );
                $notifs->setType(1);

                $entity3 = $this->manager->getManager();
                $entity3->persist($notifs);
                $entity3->flush();
            //
            
            $envoiemail = new \Swift_Message('Validation de la demande de congé du '.strval($datedebut).' au '.strval($datefin));
            $envoiemail->setFrom($emailempv);
            $envoiemail->setTo($email);
            $envoiemail->setBody(
                "<p>Bonjour,</p>
                
                Votre demande a été mise en réserve par ".$nomempv." ".$prenomempv." le ".$datenow.".
                <p>Cette demande vous sera consulter afin d'avoir une bonne entente de la validation de celle-ci.</p>  
                <p>Cordialement,</p>
                ",'text/html'
            );

            $this->mailer->send($envoiemail);
        }
        else {
            // Insert notifications
                $notifs = new Notifications();
                $notifs->setCreateur($idempvalidateur);
                $notifs->setConcerner($idempd);
                $notifs->setVue(false);
                $notifs->setDateDebut(new \DateTime($datedebut));
                $notifs->setDateFin(new \DateTime($datefin));
                $notifs->setIddemande($iddemande);
                $notifs->setNotif(
                    "Votre demande a été refusée par ".$nomempv." ".$prenomempv." le ".$datenow.""
                );
                $notifs->setType(1);

                $entity3 = $this->manager->getManager();
                $entity3->persist($notifs);
                $entity3->flush();
            //
            $envoiemail = new \Swift_Message('Validation de la demande de congé du '.strval($datedebut).' au '.strval($datefin));
            $envoiemail->setFrom($emailempv);
            $envoiemail->setTo($email);
            $envoiemail->setBody(
                '<p>Bonjour,</p>
                
                Votre demande a été refusée par '.$nomempv.' '.$prenomempv.' le '.$datenow.'
                <p>Cordialement,</p>
                ','text/html'
            );
            $this->mailer->send($envoiemail);
        }

        // return $empval;
    }

    public function annulerDemande($iddemande) {

        $succes = "";
        $echec = "";

        try {
            $sql = "update validation_demande_user set etat = -1 where iddemandeconge = :iddemande";
        
            $conn = $this->manager->getManager()->getConnection();
            $stmt = $conn->prepare($sql);
            $stmt->execute(array('iddemande' => $iddemande));
    
            $sql2 = "update demande_conge set etat = -1 where id = :iddemande";
            $stmt2 = $conn->prepare($sql2);
            $stmt2->execute(array('iddemande' => $iddemande));

            $succes = "Votre demande a été annulée avec succès";
        }
        catch(Exception $e) {
            $echec = "Votre demande n'a pas été annulé ! Erreur";
        }

        $values = array("succes" => $succes, "erreur" => $echec);

        return $values;
    }
}
?>