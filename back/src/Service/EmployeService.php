<?php

namespace App\Service;

use App\Entity\Employe;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\EmployeRepository;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Mime\Email;

class EmployeService
{
   private $emprepo;
   private $manager;
   private $_passwordEncoder;
   private $mailer;

    public function __construct(EmployeRepository $emp, ManagerRegistry $manage,  UserPasswordEncoderInterface $passwordEncoder, \Swift_Mailer $mail)
    {
        $this->emprepo = $emp;
        $this->manager = $manage;
        $this->_passwordEncoder = $passwordEncoder;
        $this->mailer = $mail;
    }

    public function getEmployeByNameAndPass($username, $password): Employe 
    {
        $employe = $this->emprepo->findOneBy([
            'email' => $username,
            'password' => $password,
        ]);

        return $employe;
    }

    public function getEmployeValidateur() : array {
        // $sql = "select id, nom, prenom, email, idposte from employe where idposte > 1 and (etat = 1 or etat is null) order by nom";
        // $conn = $this->manager->getManager()->getConnection();
        // $stmt = $conn->prepare($sql);

        // $stmt->execute();
        // return $stmt->fetchAllAssociative();

        return $this->emprepo->getEmployeValidateur();
    }

    public function getEMpById($id) 
    {
        return $this->emprepo->find($id);
    }

    public function getAllEmp() {
        // $sql = "select id, nom, prenom from employe where idposte < 3 and (etat = 1 or etat is null) order by nom";
        // $conn = $this->manager->getManager()->getConnection();
        // $stmt = $conn->prepare($sql);

        // $stmt->execute();
        // return $stmt->fetchAllAssociative();

        return $this->emprepo->getAllEmp();
    }

    public function getAllEmploye($id) {
        // $valeur = array();

        // $sql = "select id, nom, prenom from employe where id != :idempl order by nom";
        // $conn = $this->manager->getManager()->getConnection();
        // $stmt = $conn->prepare($sql);

        // $stmt->execute(array("idempl" => $id));
        // $result = $stmt->fetchAllAssociative();

        // for($i=0; $i<count($result); $i++) {
        //     $rs = array("label" => $result[$i]['nom']." ".$result[$i]['prenom'], "id" => $result[$i]['id']);
        //     array_push($valeur, $rs);
        // }

        // return $valeur;

        return $this->emprepo->getAllEmploye($id);
    }

    public function getAllUtilisateur($idemploye) {
        // $sql = "";
        // $conn = $this->manager->getManager()->getConnection();
        // $result = array();

        // if($idemploye == 0) {
        //     $sql = "select id, nom, prenom, email from employe where (etat = 1 or etat is null) order by nom";
        //     $stmt = $conn->prepare($sql);
        //     $stmt->execute();
        //     $result = $stmt->fetchAllAssociative();
        // }
        // else {
        //     $sql = "select id, nom, prenom, email from employe where id = :idemp and (etat = 1 or etat is null)";
        //     $stmt = $conn->prepare($sql);
        //     $stmt->execute(array("idemp" => $idemploye));
        //     $result = $stmt->fetchAllAssociative();
        // }

        // return $result;

        return $this->emprepo->getAllUtilisateur($idemploye);
    }

    public function getRechercheEmp() {
        // $valeur = array();

        // $t = array("label" => "Tout", "id" => 0);
        // array_push($valeur, $t);

        // $sql = "select id, nom, prenom from employe order by nom";
        // $conn = $this->manager->getManager()->getConnection();
        // $stmt = $conn->prepare($sql);

        // $stmt->execute();
        // $result = $stmt->fetchAllAssociative();

        // for($i=0; $i<count($result); $i++) {
        //     $rs = array("label" => $result[$i]['nom']." ".$result[$i]['prenom'], "id" => $result[$i]['id']);
        //     array_push($valeur, $rs);
        // }
        // return $valeur;

        return $this->emprepo->getRechercheEmp();
    }

    public function inscriptionUtilisateur($value) {
        try {
            $emp = new Employe();
            $emp->setNom($value->nom);
            $emp->setPrenom($value->prenom);
            $emp->setEmail($value->email);
            $emp->setAdresse($value->adresse);
            $emp->setTelephone($value->telephone);
            $emp->setSkype($value->skype);
            $emp->setDateEmbauche(new \DateTime($value->dateembauche));
            $emp->setIdVille($value->ville);
            $emp->setIdPoste($value->poste);
            $emp->setRoles($value->roles);
            $emp->setDateUpdate(new \DateTime());
	    $emp->setEtat(1);

            $mdp = $this->generepass();
            $emp->setPlainPassword($mdp);

            $date = $value->dateembauche;
            $datefm = date("Y-m-t", strtotime($date));

            $nbjmois = date("t", strtotime($date));

            $nbj = (strtotime($datefm)-strtotime($date))/86400;

            $sc = 2.5*$nbj/$nbjmois;

            $solde = 0;

            if($sc >=0 && $sc <=0.5) {
                $solde = 0.5;
            } elseif ($sc >0.5 && $sc <=1) {
                $solde = 1;
            } elseif ($sc >1 && $sc <=1.5) {
                $solde = 1.5;
            }elseif ($sc >1.5 && $sc <=2) {
                $solde = 2;
            }else {
                $solde = 2.5;
            }

            $emp->setSoldeConge($solde);

            if ($emp->getPlainPassword()) {
                $emp->setPassword(
                    $this->_passwordEncoder->encodePassword(
                        $emp,
                        $emp->getPlainPassword()
                    )
                );

                // $data->eraseCredentials();
            }

            $entity = $this->manager->getManager();
            $entity->persist($emp);
            $entity->flush();

            $envoiemail = new \Swift_Message('Compte Wylog+');
            $envoiemail->setFrom("ramtoky6@gmail.com");
            $envoiemail->setTo($emp->getEmail());
            $envoiemail->setBody(
                "<p>Bonjour,</p>
                <p>Votre compte Wylog+ a été créé avec succès.<br>
                    Nom d'utilisateur : ".$emp->getEmail()."<br>
                    Mot de passe : ".$emp->getPlainPassword()."<br><br>

                    Pour modifier vos informations personnelles, connectez-vous et allez dans Profil.
                </p>
                ",'text/html'
            );

            $this->mailer->send($envoiemail);

            return "Le compte a été créé avec succès";
        }
        catch(Exception $e) {
            return "Il y a une erreur!!";
        }
    }

    public function generepass() {
        $comb = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); 
        $combLen = strlen($comb) - 1; 
        for ($i = 0; $i <30; $i++) {
            $n = rand(0, $combLen);
            $pass[] = $comb[$n];
        }

        return implode($pass);
    }

    public function infoperso($id) {
        // $sql = "select e.nom, e.prenom, e.email, e.adresse, e.telephone, e.skype, v.nom ville, p.nom poste from employe e join ville v on v.id = e.idville join poste p on p.id = e.idposte where e.id = :idemp";

        // $conn = $this->manager->getManager()->getConnection();
        // $stmt = $conn->prepare($sql);

        // $stmt->execute(array("idemp" => $id));
        // $result = $stmt->fetchAssociative();

        // return $result;

        return $this->emprepo->infoperso($id);
    }

    public function modifierInfo($value) {

        $succes = "";
        $erreur = "";

        if($this->getSameEmail($value->email, $value->id)) {
            $erreur = "L'email est déjà utilisé, veuillez en saisir un autre!";
        }
        else {
            $emp = $this->emprepo->find($value->id);
            $emp->setNom($value->nom);
            $emp->setPrenom($value->prenom);
            $emp->setEmail($value->email);
            $emp->setAdresse($value->adresse);
            $emp->setTelephone($value->telephone);
            $emp->setSkype($value->skype);
            $emp->setIdVille($value->ville);
            $emp->setIdPoste($value->poste);
            $emp->setPlainPassword($value->password);

            if ($emp->getPlainPassword()) {
                $emp->setPassword(
                    $this->_passwordEncoder->encodePassword(
                        $emp,
                        $emp->getPlainPassword()
                    )
                );

                // $data->eraseCredentials();
            }

            $entity = $this->manager->getManager();
            $entity->flush();

            $succes = "Votre profil a été modifié avec succès";
        }

        return array("succes" => $succes, "erreur" => $erreur);
    }

    public function getSameEmail($email, $idemp) {
        // $sql = "select count(id) nombre from employe where email = :email and (etat = 1 or etat is null) and id != :idemp";

        // $conn = $this->manager->getManager()->getConnection();
        // $stmt = $conn->prepare($sql);
        // $stmt->execute(array("email" => $email, "idemp" => $idemp));
        // $result = $stmt->fetchAssociative();

        // if($result['nombre'] > 0) {
        //     return true;
        // }

        // return false;

        return $this->emprepo->getSameEmail($email, $idemp);
    }

    public function ficheEmp($id) {
        // $sql = "select nom, prenom, solde_conge from employe where id = :idemp";
        // $conn = $this->manager->getManager()->getConnection();
        // $stmt = $conn->prepare($sql);
        // $stmt->execute(array("idemp" => $id));
        // $result = $stmt->fetchAssociative();

        // return $result;
        return $this->emprepo->ficheEmp($id);
    }

    public function parametres($value) {
        $conn = $this->manager->getManager()->getConnection();
        $update = "update employe set solde_conge = :solde where id = :idemp";
        $stmt = $conn->prepare($update);
        $stmt->execute(array("solde" => $value->solde,"idemp" => $value->id));

        $sql = "select id, nombresjours from exceptions where idnature = :idnature and idemploye = :idemp";
        $stmt1 = $conn->prepare($sql);
        $stmt1->execute(array("idnature" => $value->idnature1,"idemp" => $value->id));
        $result1 = $stmt1->fetchAssociative();

        if($result1['nombresjours'] != null) {
            $up2 = "update exceptions set nombresjours = :nbj where id = :ide";
            $stmt3 = $conn->prepare($up2);
            $stmt3->execute(array("nbj" => $value->nbjour1, "ide" => $result1['id']));
        }
        else {
            $up2 = "insert into exceptions(idnature, nombresjours, idemploye) values('".$value->idnature1."', '".$value->nbjour1."', '".$value->id."')";
            $stmt4 = $conn->prepare($up2);
            $stmt4->execute();
        }

        $sql1 = "select id, nombresjours from exceptions where idnature = :idnature and idemploye = :idemp";
        $stmt2 = $conn->prepare($sql1);
        $stmt2->execute(array("idnature" => $value->idnature2,"idemp" => $value->id));
        $result2 = $stmt2->fetchAssociative();

        if($result2['nombresjours'] != null) {
            $up3 = "update exceptions set nombresjours = :nbj where id = :ide";
            $stmt5 = $conn->prepare($up3);
            $stmt5->execute(array("nbj" => $value->nbjour2, "ide" => $result2['id']));
        }
        else {
            $up3 = "insert into exceptions(idnature, nombresjours, idemploye) values('".$value->idnature2."', '".$value->nbjour2."', '".$value->id."')";
            $stmt4 = $conn->prepare($up3);
            $stmt4->execute();
        }

        return "Les informations sont modifiés avec succès";
    }

    public function updateSoldeConge() {
        // $conn = $this->manager->getManager()->getConnection();

        // $datefinmois = date("Y-m-t");
        // $datenow = date("Y-m-d");

        // $sql = "select date_update from employe";
        // $stmt = $conn->prepare($sql);
        // $stmt->execute();
        // $dateup = $stmt->fetchAssociative();

        // if($datenow == $datefinmois && $datenow != $dateup['date_update']) {
        //     $sql2 = "update employe e inner join employe emp on e.id = emp.id 
        //     set e.solde_conge = emp.solde_conge+2.5, e.date_update = :dateup where (e.etat = 1 or e.etat is null)";
        //     $stmt2 = $conn->prepare($sql2);
        //     $stmt2->execute(array("dateup" => $datenow));
        // }

        // return true;

        $res =  $this->emprepo->updateSoldeConge();
	return $res;
    }

    public function supprimerUser($id) {
        $emp = $this->emprepo->find($id);
        $emp->setEtat(0);
        
        $entity = $this->manager->getManager();
        $entity->flush();

        $envoiemail = new \Swift_Message('Compte Wylog+');
        $envoiemail->setFrom("ramtoky6@gmail.com");
        $envoiemail->setTo($emp->getEmail());
        $envoiemail->setBody(
            "<p>Bonjour,</p>
            <p>Votre compte Wylog+ a été supprimé.<br>
                Pour accéder au site, contactez l'administrateur pour votre réinscription.
            </p>
            ",'text/html'
        );

        $this->mailer->send($envoiemail);

        return "L'utilisateur a été supprimé avec succès";
    }
}