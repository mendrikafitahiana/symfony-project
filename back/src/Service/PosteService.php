<?php

namespace App\Service;

use App\Repository\PosteRepository;
use Doctrine\Persistence\ManagerRegistry;

class PosteService
{
    private $posterepository;
    private $manager;

    public function __construct(PosteRepository $ville, ManagerRegistry $manage)
    {
        $this->posterepository = $ville;
        $this->manager = $manage;
    }

    public function findPoste(): array{
        $villes = $this->posterepository->findAll();

        return $villes;
    }
}