<?php

namespace App\Service;

use App\Repository\DemandeCongeRepository;
use App\Repository\DemandeetvalidationRepository;
use App\Repository\EmployeRepository;
use App\Entity\DemandeConge;
use App\Entity\Employe;
use App\Repository\ValidationDemandeUserRepository;
// use App\Repository\NotificationsRepository;
use App\Entity\ValidationDemandeUser;
use App\Entity\Notifications;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class DemandeCongeService
{
    private $demanderepository;
    private $employerepository;
    private $viewrepo;
    private $manager;
    private $vdrepo;
    private $mailer;

    public function __construct(DemandeCongeRepository $demanderepo, EmployeRepository $emp, DemandeetvalidationRepository $rep, ManagerRegistry $man, ValidationDemandeUserRepository $validedemande, \Swift_Mailer $mail)
    {
        $this->demanderepository = $demanderepo;
        $this->employerepository = $emp;
        $this->viewrepo = $rep;
        $this->manager = $man;
        $this->vdrepo = $validedemande;
        $this->mailer = $mail;
    }

    public function demandeConge(DemandeConge $demande, string $bouton, array $employevalidateur)
    {
        $dateDeb = $demande->getDateDebut()->format('Y-m-d');
        $dateFin = $demande->getDateFin()->format('Y-m-d');

        $dateN = new \DateTime();
        $datenow = $dateN->format('Y-m-d');

        $emp = $this->employerepository->findOneBy(['id' => $demande->getIdemploye()]);

        $countChevauchement = $this->getNb($dateDeb, $dateFin, $demande->getIdemploye());
        $jours = $this->exceptionConge($demande->getIdnatureConge(), $demande->getIdemploye());
        $nature = $this->getNature($demande->getIdnatureConge());

        $erreur = "";
        $succes = "";

        if($bouton == "envoyer") {
        
            if($emp->getIdposte() < 3) {

                $nbJoursValides = ((((strtotime($dateDeb) - strtotime($datenow))/60)/60)/24)+1;

                if($demande->getDateDebut() > $demande->getDateFin()) {
                    $erreur =  "La date de d�but ne doit pas �tre sup�rieure � la date fin";
                }
                elseif ($countChevauchement > 0) {
                    $erreur = "Votre demande chevauche une demande existante. Merci de modifier vos dates";
                }
                elseif ($nbJoursValides < $jours) {
                    $erreur = "le '".$nature."' doit �tre pris apr�s ".$jours." jours � compter d'aujourd'hui";
                }
                // elseif ($demande->getIdnatureConge() == 2 && $nbJoursValides < 7) {
                //     $erreur = "le 'cong� de r�cup�ration' doit �tre prise apr�s 7 jours � compter d'aujourd'hui";
                // }
                elseif ($demande->getNombrejours() > $emp->getSoldeConge()) {
                    $erreur = "Votre solde de cong�s est insuffisant pour le nombre de jours demand�";
                }
                else {
                    $entity = $this->manager->getManager();
                    $entity->persist($demande);
                    $entity->flush();

                    $iddemande = $demande->getId();
                    $idempv = array();
                    $emailadress = array();

                    for($i=0; $i<count($employevalidateur); $i++) {
 
                        $empv = explode(":", $employevalidateur[$i]);
                        array_push($idempv, intval($empv[0]));
                        array_push($emailadress, $empv[1]);

                    }
                    for($j=0; $j<count($idempv); $j++) {

                        $vd = new ValidationDemandeUser();
                        $vd->setIddemandeconge($iddemande);
                        $vd->setIdemployevalidateur($idempv[$j]);
                        $vd->setEtat(2);

                        $entity2 = $this->manager->getManager();
                        $entity2->persist($vd);
                        $entity2->flush();

                        $dated = $demande->getDateDebut()->format('Y-m-d');
                        $datef = $demande->getDateFin()->format('Y-m-d');

                        // Insert notifications
                            $notif  = new Notifications();
                            $notif->setCreateur($demande->getIdemploye());
                            $notif->setConcerner($idempv[$j]);
                            $notif->setVue(false);
                            $notif->setDateDebut($demande->getDateDebut());
                            $notif->setDateFin($demande->getDateFin());
                            $notif->setIddemande($iddemande);
                            $notif->setNotif(
                                "Vous avez une demande de cong�s envoy�e par ".$emp->getNom()." ".$emp->getPrenom()."
                                 pour le ".$dated." au ".$datef."
                                 le ".$datenow.""
                            );
                            $notif->setType(0);

                            $entity3 = $this->manager->getManager();
                            $entity3->persist($notif);
                            $entity3->flush();
                        // 

                        

                        $envoiemail = new \Swift_Message('Demande de cong�s du '.strval($dated).' au '.strval($datef));
                        $envoiemail->setFrom($emp->getEmail());
                        $envoiemail->setTo($emailadress);
                        $envoiemail->setBody(
                            '<p>Bonjour,</p>
                            
                           <p>Sujet: Demande de cong�s du '.strval($dated).' au '.strval($datef).'</p>
                            <p>Nature : '.$nature.'</p>
                           <p>Concern�(e) : '.$emp->getNom().' '.$emp->getPrenom().'</p>
                           <p>Nombre de jours : '.strval($demande->getNombrejours()).'</p>
                           <p>'.$demande->getCommentaires().'</p>
                            <br/>
                            <p>Coordialement,</p>'
                            ,'text/html'
                        );

                    }
                    
                    $this->mailer->send($envoiemail);

                    $succes = "Succ�s ! Votre demande a bien �t� envoy�e";
                }
            } 
            else {

                if($demande->getDateDebut() > $demande->getDateFin()) {
                    $erreur =  "La date de d�but ne doit pas �tre sup�rieure � la date fin";
                }
                elseif ($countChevauchement > 0) {
                    $erreur = "Votre demande chevauche une demande existante. Merci de modifier vos dates";
                }
                else {
                    $entity = $this->manager->getManager();
                    $entity->persist($demande);
                    $entity->flush();

                    $iddemande = $demande->getId();
                    $idempv = array();
                    $emailadress = array();

                    for($i=0; $i<count($employevalidateur); $i++) {
 
                        $empv = explode(":", $employevalidateur[$i]);
                        array_push($idempv, intval($empv[0]));
                        array_push($emailadress, $empv[1]);

                    }
                    for($j=0; $j<count($idempv); $j++) {

                        $vd = new ValidationDemandeUser();
                        $vd->setIddemandeconge($iddemande);
                        $vd->setIdemployevalidateur($idempv[$j]);
                        $vd->setEtat(2);

                        $entity2 = $this->manager->getManager();
                        $entity2->persist($vd);
                        $entity2->flush();
                        
                        $dated = $demande->getDateDebut()->format('Y-m-d');
                        $datef = $demande->getDateFin()->format('Y-m-d');

                        // Insert notifications
                        $notif  = new Notifications();
                        $notif->setCreateur($demande->getIdemploye());
                        $notif->setConcerner($idempv[$j]);
                        $notif->setVue(false);
                        $notif->setDateDebut($demande->getDateDebut());
                        $notif->setDateFin($demande->getDateFin());
                        $notif->setIddemande($iddemande);
                        $notif->setNotif(
                            "Vous avez une demande de cong�s envoy�e par ".$emp->getNom()." ".$emp->getPrenom()."
                             pour le ".$dated." au ".$datef."
                             le ".$datenow.""
                        );
                        $notif->setType(0);

                        $entity3 = $this->manager->getManager();
                        $entity3->persist($notif);
                        $entity3->flush();
                        // 

                        $envoiemail = new \Swift_Message('Demande de cong�s du '.strval($dated).' au '.strval($datef));
                        $envoiemail->setFrom($emp->getEmail());
                        $envoiemail->setTo($emailadress);
                        $envoiemail->setBody(
                            '<p>Bonjour,</p>
                            
                           <p>Sujet: Demande de cong�s du '.strval($dated).' au '.strval($datef).'</p>
                            <p>Nature : '.$nature.'</p>
                           <p>Concern�(e) : '.$emp->getNom().' '.$emp->getPrenom().'</p>
                           <p>Nombre de jours : '.strval($demande->getNombrejours()).'</p>
                           <p>'.$demande->getCommentaires().'</p>
                            <br/>
                            <p>Coordialement,</p>'
                            ,'text/html'
                        );

                    }
                    
                    $this->mailer->send($envoiemail);

                    $succes = "Succ�s ! Votre demande a bien �t� envoy�e";
                }
            }

        }
        else {
            if($emp->getIdposte() < 3) {

                $nbJoursValides = ((((strtotime($dateDeb) - strtotime($datenow))/60)/60)/24)+1;

                if($demande->getDateDebut() > $demande->getDateFin()) {
                    $erreur =  "La date de d�but ne doit pas �tre sup�rieure � la date fin";
                }
                elseif ($countChevauchement > 0) {
                    $erreur = "Votre demande chevauche une demande existante. Merci de modifier vos dates";
                }
                elseif ($nbJoursValides < $jours) {
                    $erreur = "le '".$nature."' doit �tre pris apr�s ".$jours." jours � compter d'aujourd'hui";
                }
                // elseif ($demande->getIdnatureConge() == 2 && $nbJoursValides < 7) {
                //     $erreur = "le 'cong� de r�cup�ration' doit �tre prise apr�s 7 jours � compter d'aujourd'hui";
                // }
                elseif ($demande->getNombrejours() > $emp->getSoldeConge()) {
                    $erreur = "Votre solde de cong�s est insuffisant pour le nombre de jours demand�";
                }
                else {
                    $entity = $this->manager->getManager();
                    $entity->persist($demande);
                    $entity->flush();

                    $iddemande = $demande->getId();
                    $idempv = array();

                    for($i=0; $i<count($employevalidateur); $i++) {
 
                        $empv = explode(":", $employevalidateur[$i]);
                        array_push($idempv, $empv[0]);

                    }
                    for($j=0; $j<count($idempv); $j++) {

                        $vd = new ValidationDemandeUser();
                        $vd->setIddemandeconge($iddemande);
                        $vd->setIdemployevalidateur($idempv[$j]);
                        $vd->setEtat(1);

                        $entity2 = $this->manager->getManager();
                        $entity2->persist($vd);
                        $entity2->flush();

                    }
                    $succes = "Succ�s ! Votre demande a bien �t� enregistr�e";
                }
            } 
            else {

                if($demande->getDateDebut() > $demande->getDateFin()) {
                    $erreur =  "La date de d�but ne doit pas �tre sup�rieure � la date fin";
                }
                elseif ($countChevauchement > 0) {
                    $erreur = "Votre demande chevauche une demande existante. Merci de modifier vos dates";
                }
                else {
                    $entity = $this->manager->getManager();
                    $entity->persist($demande);
                    $entity->flush();

                    $iddemande = $demande->getId();
                    $idempv = array();

                    for($i=0; $i<count($employevalidateur); $i++) {
 
                        $empv = explode(":", $employevalidateur[$i]);
                        array_push($idempv, $empv[0]);

                    }
                    for($j=0; $j<count($idempv); $j++) {

                        $vd = new ValidationDemandeUser();
                        $vd->setIddemandeconge($iddemande);
                        $vd->setIdemployevalidateur($idempv[$j]);
                        $vd->setEtat(1);

                        $entity2 = $this->manager->getManager();
                        $entity2->persist($vd);
                        $entity2->flush();

                    }
                    $succes = "Succ�s ! Votre demande a bien �t� enregistr�e";
                }
            }
        }
        

        $values = array("succes" => $succes, "erreur" => $erreur);
        return $values;
    }




    public function getNb($dateDeb, $dateFin, $emp) 
    {
        $sql = "select count(*) nombre from demandeetvalidation t0 where ((t0.date_debut = :dd or t0.date_fin = :df) and (t0.etat_validation is null or t0.etat_validation != -1 or t0.etat_validation != 4) and t0.idemploye = :ide)";

        $conn = $this->manager->getManager()->getConnection();
        $stmt = $conn->prepare($sql);
    
        // $res = $stmt->execute(array('dd' => $dateDeb, 'df' => $dateFin, 'ide' => $emp));
        $stmt->execute(array('dd' => $dateDeb, 'df' => $dateFin, 'ide' => $emp));
        // $aa = $res->fetchAssociative();
        $aa = $stmt->fetchAssociative();
        return $aa['nombre'];
    }

    public function getNbModif($dateDeb, $dateFin, $emp, $iddemande) 
    {
        $sql = "select count(*) nombre from demandeetvalidation t0 where ((t0.date_debut = :dd or t0.date_fin = :df) and (t0.etat_validation is null or t0.etat_validation != -1 or t0.etat_validation != 4) and t0.idemploye = :ide and id != :iddemande)";

        $conn = $this->manager->getManager()->getConnection();
        $stmt = $conn->prepare($sql);
    
        // $res = $stmt->execute(array('dd' => $dateDeb, 'df' => $dateFin, 'ide' => $emp, 'iddemande' => $iddemande));
        $stmt->execute(array('dd' => $dateDeb, 'df' => $dateFin, 'ide' => $emp, 'iddemande' => $iddemande));
        // $aa = $res->fetchAssociative();
        $aa = $stmt->fetchAssociative();
        return $aa['nombre'];
    }

    public function envoiApresEnregistrement($iddemande) 
    {
        $message = "";
        $dateN = new \DateTime();
        $datenow = $dateN->format('Y-m-d');
        
        try {
            $validationdemande = $this->vdrepo->findBy(["iddemandeconge" => $iddemande]);
            $demande = $this->demanderepository->find($iddemande);
            $emp = $this->employerepository->findOneBy(['id' => $demande->getIdemploye()]);
            $nature = $this->getNature($demande->getIdnatureConge());
    
            $entity = $this->manager->getManager();
    
            $demande->setEtat(2);

            $dated = $demande->getDateDebut()->format('Y-m-d');
            $datef = $demande->getDateFin()->format('Y-m-d');

            $entity->flush();
            
            $emailadress = array();

            for($i=0; $i<count($validationdemande); $i++) {
                $validationdemande[$i]->setEtat(2);
                $entity->flush();

                // Insert notifications
                    $notif  = new Notifications();
                    $notif->setCreateur($demande->getIdemploye());
                    $notif->setConcerner($validationdemande[$i]->getIdemployevalidateur());
                    $notif->setVue(false);
                    $notif->setDateDebut($demande->getDateDebut());
                    $notif->setDateFin($demande->getDateFin());
                    $notif->setIddemande($iddemande);
                    $notif->setNotif(
                        "Vous avez une demande de cong�s envoy�e par ".$emp->getNom()." ".$emp->getPrenom()."
                        pour le ".$dated." au ".$datef."
                         le ".$datenow.""
                    );
                    $notif->setType(0);

                    $entity3 = $this->manager->getManager();
                    $entity3->persist($notif);
                    $entity3->flush();
                // 

                $empv = $this->employerepository->find($validationdemande[$i]->getIdemployevalidateur());
                array_push($emailadress, $empv->getEmail());
            }

            $envoiemail = new \Swift_Message('Demande de cong�s du '.strval($dated).' au '.strval($datef));
            $envoiemail->setFrom($emp->getEmail());
            $envoiemail->setTo($emailadress);
            $envoiemail->setBody(
                '<p>Bonjour,</p>
                
                <p>Sujet: Demande de cong�s du '.strval($dated).' au '.strval($datef).'</p>
                <p>Nature : '.$nature.'</p>
                <p>Concern�: '.$emp->getNom().' '.$emp->getPrenom().'</p>
                <p>Nombre de jours : '.strval($demande->getNombrejours()).'</p>
                <p>'.$demande->getCommentaires().'</p>
                <p>Coordialement,</p>'
                ,'text/html'
            );

            $this->mailer->send($envoiemail);

            $message = "succ�s";
        }
        catch(Exception $e) {
            $message = $e->getMessage();
        }
        return $message;
    }

    public function ModifierDemandeConge($value)
    {
        $conn = $this->manager->getManager()->getConnection();

        $emp = $this->employerepository->findOneBy(['id' => $value->idemploye]);

        $countChevauchement = $this->getNbModif($value->datedebut, $value->datefin, $value->idemploye, $value->id);

        $erreur = "";
        $succes = "";

        $dateN = new \DateTime();
        $datenow = $dateN->format('Y-m-d');
        $jours = $this->exceptionConge($value->idnature, $value->idemploye);
        $nature = $this->getNature($value->idnature);

        if($value->bouton == "envoyer") {

            if($emp->getIdposte() < 3) {

                $nbJoursValides = ((((strtotime($value->datedebut) - strtotime($datenow))/60)/60)/24)+1;
                
                if(new \DateTime($value->datedebut) > new \DateTime($value->datefin)) {
                    $erreur =  "La date de d�but ne doit pas �tre sup�rieure � la date fin";
                }
                elseif($countChevauchement > 0) {
                    $erreur = "Votre demande chevauche une demande existante. Merci de modifier vos dates";
                }
                elseif ($nbJoursValides < $jours) {
                    $erreur = "le '".$nature."' doit �tre pris apr�s ".$jours." jours � compter d'aujourd'hui";
                }
                // elseif ($value->idnature == 2 && $nbJoursValides < 7) {
                //     $erreur = "le 'cong� de r�cup�ration' doit �tre prise apr�s 7 jours � compter d'aujourd'hui";
                // }
                elseif ($value->nombrejours > $emp->getSoldeConge()) {
                    $erreur = "Votre solde de cong�s est insuffisant pour le nombre de jours demand�";
                } 
                else {
                    $sql = "update demande_conge set date_debut = :datedebut, date_fin = :datefin, idnature_conge = :idnature, etat = :etat, nombrejours = :nombrejour, commentaires = :commentaires, createdat = current_timestamp where id = :iddemande";
                    $stmt = $conn->prepare($sql);

                    $stmt->execute(array("datedebut" => $value->datedebut, "datefin" => $value->datefin, "idnature" => $value->idnature, "etat" => 2, "nombrejour" => $value->nombrejours, "commentaires" => $value->commentaires, "iddemande" => $value->id));

                    $sql2 = "update validation_demande_user set etat = :etat where iddemandeconge = :iddemande";
                    $stmt2 = $conn->prepare($sql2);

                    $stmt2->execute(array("etat" => 2, "iddemande" => $value->id));

                    $empval = $this->getIdEmailEmpV($value->id);
                    $emailadress = array();

                    for($i=0; $i<count($empval); $i++) {
                        array_push($emailadress,$empval[$i]['email']);

                        // Insertion notifications
                            $notif = new Notifications();
                            $notif->setCreateur($value->idemploye);
                            $notif->setConcerner($empval[$i]['id']);
                            $notif->setVue(false);
                            $notif->setDateDebut(new \DateTime($value->datedebut));
                            $notif->setDateFin(new \DateTime($value->datefin));
                            $notif->setIddemande($value->id);
                            $notif->setNotif(
                                "La demande de cong�s du ".$value->datedebut." au ".$value->datefin." 
                                a �t� modifi�e et envoy�e par ".$emp->getNom()." ".$emp->getPrenom()."
                                le ".$datenow.""
                            );
                            $notif->setType(0);

                            $entity = $this->manager->getManager();
                            $entity->persist($notif);
                            $entity->flush();
                        // 
                    }

                    $envoiemail = new \Swift_Message("Demande de cong�s du ".$value->datedebut." au ".$value->datefin);
                    $envoiemail->setFrom($emp->getEmail());
                    $envoiemail->setTo($emailadress);
                    $envoiemail->setBody(
                        '<p>Bonjour,</p>
                        <p>Cette demande de cong�s a �t� modifi�e et envoy�e par '.$emp->getNom().' '.$emp->getPrenom().'
                        le '.$datenow.'</p>
                        <p>Sujet : Demande de cong�s du '.$value->datedebut.' au '.$value->datefin.'</p>
                       <p>Nature : '.$nature.'</p> 
                       <p>Concern�(e) : '.$emp->getNom().' '.$emp->getPrenom().'</p>
                        <p>Nombre de jours : '.$value->nombrejours.'</p>
                        <p>'.$demande->getCommentaires().'</p>
                        <br/>
                        <p>Cordialement,</p>'
                        ,'text/html'
                    );

                    $this->mailer->send($envoiemail);

                    $succes = "Succ�s ! Votre demande a bien �t� modif�e et envoy�e";
                }
            }
            else {
                if(new \DateTime($value->datedebut) > new \DateTime($value->datefin)) {
                    $erreur =  "La date de d�but ne doit pas �tre sup�rieure � la date fin";
                }
                elseif ($countChevauchement > 0) {
                    $erreur = "Votre demande chevauche une demande existante. Merci de modifier vos dates";
                }
                else {
                    $sql = "update demande_conge set date_debut = :datedebut, date_fin = :datefin, idnature_conge = :idnature, etat = :etat, nombrejours = :nombrejour, commentaires = :commentaires, createdat = current_timestamp where id = :iddemande";
                    $stmt = $conn->prepare($sql);

                    $stmt->execute(array("datedebut" => $value->datedebut, "datefin" => $value->datefin, "idnature" => $value->idnature, "etat" => 2, "nombrejour" => $value->nombrejours, "commentaires" => $value->commentaires, "iddemande" => $value->id));

                    $sql2 = "update validation_demande_user set etat = :etat where iddemandeconge = :iddemande";
                    $stmt2 = $conn->prepare($sql2);

                    $stmt2->execute(array("etat" => 2, "iddemande" => $value->id));

                    $empval = $this->getIdEmailEmpV($value->id);
                    $emailadress = array();

                    for($i=0; $i<count($empval); $i++) {
                        array_push($emailadress,$empval[$i]['email']);

                        // Insertion notifications
                            $notif = new Notifications();
                            $notif->setCreateur($value->idemploye);
                            $notif->setConcerner($empval[$i]['id']);
                            $notif->setVue(false);
                            $notif->setDateDebut(new \DateTime($value->datedebut));
                            $notif->setDateFin(new \DateTime($value->datefin));
                            $notif->setIddemande($value->id);
                            $notif->setNotif(
                                "La demande de cong�s du ".$value->datedebut." au ".$value->datefin." 
                                a �t� modifi�e et envoy�e par ".$emp->getNom()." ".$emp->getPrenom()."
                                le ".$datenow.""
                            );
                            $notif->setType(0);

                            $entity = $this->manager->getManager();
                            $entity->persist($notif);
                            $entity->flush();
                        // 
                    }

                    $envoiemail = new \Swift_Message("Demande de cong�s du ".$value->datedebut." au ".$value->datefin);
                    $envoiemail->setFrom($emp->getEmail());
                    $envoiemail->setTo($emailadress);
                    $envoiemail->setBody(
                        '<p>Bonjour,</p>
                        <p>Cette demande de cong�s a �t� modifi�e et envoy�e par '.$emp->getNom().' '.$emp->getPrenom().'
                        le '.$datenow.'</p>
                        <p>Sujet : Demande de cong�s du '.$value->datedebut.' au '.$value->datefin.'</p>
                       <p>Nature : '.$nature.'</p> 
                       <p>Concern�(e) : '.$emp->getNom().' '.$emp->getPrenom().'</p>
                        <p>Nombre de jours : '.$value->nombrejours.'</p>
                        <p>'.$demande->getCommentaires().'</p>
                        <br/>
                        <p>Cordialement,</p>'
                        ,'text/html'
                    );

                    $this->mailer->send($envoiemail);

                    $succes = "Succ�s ! Votre demande a bien �t� modif�e et envoy�e";
                }
            }
        }
        else {
            if($emp->getIdposte() < 3) {

                $nbJoursValides = ((((strtotime($value->datedebut) - strtotime($datenow))/60)/60)/24)+1;
                
                if(new \DateTime($value->datedebut) > new \DateTime($value->datefin)) {
                    $erreur =  "La date de d�but ne doit pas �tre sup�rieure � la date fin";
                }
                elseif($countChevauchement > 0) {
                    $erreur = "Votre demande chevauche une demande existante. Merci de modifier vos dates";
                }
                elseif ($nbJoursValides < $jours) {
                    $erreur = "le '".$nature."' doit �tre pris apr�s ".$jours." jours � compter d'aujourd'hui";
                }
                // elseif ($value->idnature == 2 && $nbJoursValides < 7) {
                //     $erreur = "le 'cong� de r�cup�ration' doit �tre prise apr�s 7 jours � compter d'aujourd'hui";
                // }
                elseif ($value->nombrejours > $emp->getSoldeConge()) {
                    $erreur = "Votre solde de cong�s est insuffisant pour le nombre de jours demand�";
                } 
                else {
                    $sql = "update demande_conge set date_debut = :datedebut, date_fin = :datefin, idnature_conge = :idnature, etat = :etat, nombrejours = :nombrejour, commentaires = :commentaires, createdat = current_timestamp where id = :iddemande";
                    $stmt = $conn->prepare($sql);

                    $stmt->execute(array("datedebut" => $value->datedebut, "datefin" => $value->datefin, "idnature" => $value->idnature, "etat" => 1, "nombrejour" => $value->nombrejours, "commentaires" => $value->commentaires, "iddemande" => $value->id));

                    $sql2 = "update validation_demande_user set etat = :etat where iddemandeconge = :iddemande";
                    $stmt2 = $conn->prepare($sql2);

                    $stmt2->execute(array("etat" => 1, "iddemande" => $value->id));

                    $succes = "Succ�s ! Votre demande a bien �t� modif�e et enregistr�e";
                }
            }
            else {
                if(new \DateTime($value->datedebut) > new \DateTime($value->datefin)) {
                    $erreur =  "La date de d�but ne doit pas �tre sup�rieure � la date fin";
                }
                elseif ($countChevauchement > 0) {
                    $erreur = "Votre demande chevauche une demande existante. Merci de modifier vos dates";
                }
                else {
                    $sql = "update demande_conge set date_debut = :datedebut, date_fin = :datefin, idnature_conge = :idnature, etat = :etat, nombrejours = :nombrejour, commentaires = :commentaires, createdat = current_timestamp where id = :iddemande";
                    $stmt = $conn->prepare($sql);

                    $stmt->execute(array("datedebut" => $value->datedebut, "datefin" => $value->datefin, "idnature" => $value->idnature, "etat" => 1, "nombrejour" => $value->nombrejours, "commentaires" => $value->commentaires, "iddemande" => $value->id));

                    $sql2 = "update validation_demande_user set etat = :etat where iddemandeconge = :iddemande";
                    $stmt2 = $conn->prepare($sql2);

                    $stmt2->execute(array("etat" => 1, "iddemande" => $value->id));

                    $succes = "Succ�s ! Votre demande a bien �t� modif�e et enregistr�e";
                }
            } 
        }

        $values = array("succes" => $succes, "erreur" => $erreur);
        return $values;
    }

    public function findCongeById($iddemande) 
    {
        $demande = $this->demanderepository->find($iddemande);

        return $demande;
    }

    public function getIdEmailEmpV($iddemande) {
        $sql = "select e.id, e.email from employe e join validation_demande_user vd on vd.idemployevalidateur = e.id where vd.iddemandeconge = :iddemande";

        $conn = $this->manager->getManager()->getConnection();
        $stmt = $conn->prepare($sql);
        // $res = $stmt->execute(array("iddemande" => $iddemande));
        $stmt->execute(array("iddemande" => $iddemande));

        // $result = $res->fetchAllAssociative();
        $result = $stmt->fetchAllAssociative();

        return $result;
    }

    public function exceptionConge($idnature, $idemp) {
        $conn = $this->manager->getManager()->getConnection();
        $valiny = 0;
        if($idnature == 1) {
            $sql = "select nombresjours from exceptions where idemploye = :idemp and idnature = :idnature";
            $stmt = $conn->prepare($sql);
            // $res = $stmt->execute(array("idemp" => $idemp, "idnature" => $idnature));
            $stmt->execute(array("idemp" => $idemp, "idnature" => $idnature));

            // $result = $res->fetchAssociative();
            $result = $stmt->fetchAssociative();

            if($result != null) {
                $valiny = $result['nombresjours'];
            }
            else {
                $valiny = 15;
            }
        }
        if($idnature == 2) {
            $sql = "select nombresjours from exceptions where idemploye = :idemp and idnature = :idnature";
            $stmt = $conn->prepare($sql);
             // $res = $stmt->execute(array("idemp" => $idemp, "idnature" => $idnature));
             $stmt->execute(array("idemp" => $idemp, "idnature" => $idnature));

             // $result = $res->fetchAssociative();
             $result = $stmt->fetchAssociative();
            if($result != null) {
                $valiny = $result['nombresjours'];
            }
            else {
                $valiny = 7;
            }
        }

        return $valiny;
    }

    public function getNature($idnature) {
        $sql = "select nom from nature where id = :idnature";
        $conn = $this->manager->getManager()->getConnection();
        $stmt = $conn->prepare($sql);
        // $res = $stmt->execute(array("idnature" => $idnature));
        $stmt->execute(array("idnature" => $idnature));

        // $result = $res->fetchAssociative();
        $result = $stmt->fetchAssociative();

        return $result['nom'];
    }
}