<?php

namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationFailureResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

class AuthenticationFailureListener {

    /**
     * @param AuthenticationFailureEvent $event
     */
    public function onAuthenticationFailureResponse(AuthenticationFailureEvent $event)
    {
        // $data = $event->getData();

        $response = new JWTAuthenticationFailureResponse('Utilisateur non validé, vérifiez votre adresse email ou mot de passe', JsonResponse::HTTP_UNAUTHORIZED);
        // $response->setData($data);

        $event->setResponse($response);
    }
}