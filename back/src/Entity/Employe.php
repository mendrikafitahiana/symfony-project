<?php

namespace App\Entity;

use App\Repository\EmployeRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=EmployeRepository::class)
 * 
 * @ApiResource(
 *      normalizationContext={"groups"={"user:read"}},
 *      denormalizationContext={"groups"={"user:write"}}
 * )
 */
class Employe implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * 
     * @Groups("user:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * 
     * @Groups({"user:read", "user:write"})
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     * 
     * @Groups({"user:read", "user:write"})
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=50)
     * 
     * @Groups({"user:read", "user:write"})
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=50)
     * 
     * @Groups({"user:read", "user:write"})
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=50)
     * 
     * @Groups({"user:read", "user:write"})
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * 
     * @Groups({"user:read", "user:write"})
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * 
     * @Groups({"user:read", "user:write"})
     */
    private $skype;

    /**
     * @ORM\Column(type="date")
     * 
     * @Groups({"user:read", "user:write"})
     */
    private $dateEmbauche;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=2)
     * 
     * @Groups({"user:read", "user:write"})
     */
    private $soldeConge;

    /**
     * @ORM\Column(type="integer")
     * 
     * @Groups({"user:read", "user:write"})
     */
    private $idville;

    /**
     * @ORM\Column(type="integer")
     * 
     * @Groups({"user:read", "user:write"})
     */
    private $idposte;

    /**
     * @Groups({"user:read", "user:write"})
     * 
     * @SerializedName("password")
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="date")
     */
    private $DateUpdate;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * 
     * @Groups({"user:read", "user:write"})
     */
    private $Etat;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = null;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getSkype(): ?string
    {
        return $this->skype;
    }

    public function setSkype(?string $skype): self
    {
        $this->skype = $skype;

        return $this;
    }

    public function getDateEmbauche(): ?\DateTimeInterface
    {
        return $this->dateEmbauche;
    }

    public function setDateEmbauche(\DateTimeInterface $dateEmbauche): self
    {
        $this->dateEmbauche = $dateEmbauche;

        return $this;
    }

    public function getSoldeConge(): ?string
    {
        return $this->soldeConge;
    }

    public function setSoldeConge(string $soldeConge): self
    {
        $this->soldeConge = $soldeConge;

        return $this;
    }

    public function getIdville(): ?int
    {
        return $this->idville;
    }

    public function setIdville(int $idville): self
    {
        $this->idville = $idville;

        return $this;
    }

    public function getIdposte(): ?int
    {
        return $this->idposte;
    }

    public function setIdposte(int $idposte): self
    {
        $this->idposte = $idposte;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    public function getDateUpdate(): ?\DateTimeInterface
    {
        return $this->DateUpdate;
    }

    public function setDateUpdate(\DateTimeInterface $DateUpdate): self
    {
        $this->DateUpdate = $DateUpdate;

        return $this;
    }

    public function getEtat(): ?int
    {
        return $this->Etat;
    }

    public function setEtat(?int $Etat): self
    {
        $this->Etat = $Etat;

        return $this;
    }
}
