<?php

namespace App\Entity;

use App\Repository\NotificationsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NotificationsRepository::class)
 */
class Notifications
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $vuLe;

    /**
     * @ORM\Column(type="boolean")
     */
    private $vue;

    /**
     * @ORM\Column(type="integer")
     */
    private $createur;

    /**
     * @ORM\Column(type="integer")
     */
    private $concerner;

    /**
     * @ORM\Column(type="text")
     */
    private $notif;

    /**
     * @ORM\Column(type="date")
     */
    private $dateDebut;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateFin;

    /**
     * @ORM\Column(type="integer")
     */
    private $iddemande;

    /**
     * @ORM\Column(type="integer")
     */
    private $Type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVuLe(): ?\DateTimeInterface
    {
        return $this->vuLe;
    }

    public function setVuLe(?\DateTimeInterface $vuLe): self
    {
        $this->vuLe = $vuLe;

        return $this;
    }

    public function isVue(): ?bool
    {
        return $this->vue;
    }

    public function setVue(bool $vue): self
    {
        $this->vue = $vue;

        return $this;
    }

    public function getCreateur(): ?int
    {
        return $this->createur;
    }

    public function setCreateur(int $createur): self
    {
        $this->createur = $createur;

        return $this;
    }

    public function getConcerner(): ?int
    {
        return $this->concerner;
    }

    public function setConcerner(int $concerner): self
    {
        $this->concerner = $concerner;

        return $this;
    }

    public function getNotif(): ?string
    {
        return $this->notif;
    }

    public function setNotif(string $notif): self
    {
        $this->notif = $notif;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(\DateTimeInterface $dateDebut): self
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getIddemande(): ?int
    {
        return $this->iddemande;
    }

    public function setIddemande(int $iddemande): self
    {
        $this->iddemande = $iddemande;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->Type;
    }

    public function setType(int $Type): self
    {
        $this->Type = $Type;

        return $this;
    }
}
