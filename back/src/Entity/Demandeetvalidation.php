<?php

namespace App\Entity;

use App\Repository\DemandeetvalidationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DemandeetvalidationRepository::class)
 */
class Demandeetvalidation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $dateDebut;

    /**
     * @ORM\Column(type="date")
     */
    private $dateFin;

    /**
     * @ORM\Column(type="integer")
     */
    private $idnatureConge;

    /**
     * @ORM\Column(type="text")
     */
    private $commentaires;

    /**
     * @ORM\Column(type="integer")
     */
    private $idemploye;

    /**
     * @ORM\Column(type="integer")
     */
    private $etatDemande;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=2)
     */
    private $nombrejours;

    /**
     * @ORM\Column(type="integer")
     */
    private $etatValidation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(\DateTimeInterface $dateDebut): self
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getIdnatureConge(): ?int
    {
        return $this->idnatureConge;
    }

    public function setIdnatureConge(int $idnatureConge): self
    {
        $this->idnatureConge = $idnatureConge;

        return $this;
    }

    public function getCommentaires(): ?string
    {
        return $this->commentaires;
    }

    public function setCommentaires(string $commentaires): self
    {
        $this->commentaires = $commentaires;

        return $this;
    }

    public function getIdemploye(): ?int
    {
        return $this->idemploye;
    }

    public function setIdemploye(int $idemploye): self
    {
        $this->idemploye = $idemploye;

        return $this;
    }

    public function getEtatDemande(): ?int
    {
        return $this->etatDemande;
    }

    public function setEtatDemande(int $etatDemande): self
    {
        $this->etatDemande = $etatDemande;

        return $this;
    }

    public function getNombrejours(): ?string
    {
        return $this->nombrejours;
    }

    public function setNombrejours(string $nombrejours): self
    {
        $this->nombrejours = $nombrejours;

        return $this;
    }

    public function getEtatValidation(): ?int
    {
        return $this->etatValidation;
    }

    public function setEtatValidation(int $etatValidation): self
    {
        $this->etatValidation = $etatValidation;

        return $this;
    }
}
