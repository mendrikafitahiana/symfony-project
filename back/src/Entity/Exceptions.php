<?php

namespace App\Entity;

use App\Repository\ExceptionsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ExceptionsRepository::class)
 */
class Exceptions
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $idnature;

    /**
     * @ORM\Column(type="integer")
     */
    private $nombresjours;

    /**
     * @ORM\Column(type="integer")
     */
    private $idemploye;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdnature(): ?int
    {
        return $this->idnature;
    }

    public function setIdnature(int $idnature): self
    {
        $this->idnature = $idnature;

        return $this;
    }

    public function getNombresjours(): ?int
    {
        return $this->nombresjours;
    }

    public function setNombresjours(int $nombresjours): self
    {
        $this->nombresjours = $nombresjours;

        return $this;
    }

    public function getIdemploye(): ?int
    {
        return $this->idemploye;
    }

    public function setIdemploye(int $idemploye): self
    {
        $this->idemploye = $idemploye;

        return $this;
    }
}
