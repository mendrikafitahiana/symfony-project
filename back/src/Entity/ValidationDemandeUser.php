<?php

namespace App\Entity;

use App\Repository\ValidationDemandeUserRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ValidationDemandeUserRepository::class)
 */
class ValidationDemandeUser
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $iddemandeconge;

    /**
     * @ORM\Column(type="integer")
     */
    private $idemployevalidateur;

    /**
     * @ORM\Column(type="integer")
     */
    private $etat;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $CommentairesValidation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIddemandeconge(): ?int
    {
        return $this->iddemandeconge;
    }

    public function setIddemandeconge(int $iddemandeconge): self
    {
        $this->iddemandeconge = $iddemandeconge;

        return $this;
    }

    public function getIdemployevalidateur(): ?int
    {
        return $this->idemployevalidateur;
    }

    public function setIdemployevalidateur(int $idemployevalidateur): self
    {
        $this->idemployevalidateur = $idemployevalidateur;

        return $this;
    }

    public function getEtat(): ?int
    {
        return $this->etat;
    }

    public function setEtat(int $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getCommentairesValidation(): ?string
    {
        return $this->CommentairesValidation;
    }

    public function setCommentairesValidation(?string $CommentairesValidation): self
    {
        $this->CommentairesValidation = $CommentairesValidation;

        return $this;
    }
}
