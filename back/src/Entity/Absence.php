<?php

namespace App\Entity;

use App\Repository\AbsenceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AbsenceRepository::class)
 */
class Absence
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $dateabsence;

    /**
     * @ORM\Column(type="integer")
     */
    private $idemploye;

    /**
     * @ORM\Column(type="datetime")
     */
    private $Createdat;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateabsence(): ?\DateTimeInterface
    {
        return $this->dateabsence;
    }

    public function setDateabsence(\DateTimeInterface $dateabsence): self
    {
        $this->dateabsence = $dateabsence;

        return $this;
    }

    public function getIdemploye(): ?int
    {
        return $this->idemploye;
    }

    public function setIdemploye(int $idemploye): self
    {
        $this->idemploye = $idemploye;

        return $this;
    }

    public function getCreatedat(): ?\DateTimeInterface
    {
        return $this->Createdat;
    }

    public function setCreatedat(\DateTimeInterface $Createdat): self
    {
        $this->Createdat = $Createdat;

        return $this;
    }
}
