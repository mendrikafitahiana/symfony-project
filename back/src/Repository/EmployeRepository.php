<?php

namespace App\Repository;

use App\Entity\Employe;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

/**
 * @extends ServiceEntityRepository<Employe>
 *
 * @method Employe|null find($id, $lockMode = null, $lockVersion = null)
 * @method Employe|null findOneBy(array $criteria, array $orderBy = null)
 * @method Employe[]    findAll()
 * @method Employe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmployeRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Employe::class);
    }

    public function add(Employe $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Employe $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof Employe) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newHashedPassword);

        $this->add($user, true);
    }

//    /**
//     * @return Employe[] Returns an array of Employe objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Employe
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }

    public function updateSoldeConge() {
        $conn = $this->getEntityManager()->getConnection();
        $datefinmois = date("Y-m-t");
        $datenow = date("Y-m-d");

        $sql = "select date_update from employe";
        $stmt = $conn->prepare($sql);
        $resultSet = $stmt->executeQuery();

        $dateup = $resultSet->fetchAssociative();
        if($datenow == $datefinmois && $datenow != $dateup['date_update']) {
            $sql2 = "update employe e inner join employe emp on e.id = emp.id 
            set e.solde_conge = emp.solde_conge+2.5, e.date_update = :dateup where e.etat = 1";
            $stmt2 = $conn->prepare($sql2);
            $stmt2->executeQuery(array("dateup" => $datenow));
        }

        return true;
    }

    public function getEmployeValidateur() {
        $sql = "select id, nom, prenom, email, idposte from employe where idposte > 1 and etat = 1 order by nom";
        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);

        $res = $stmt->executeQuery();
        return $res->fetchAllAssociative();
    }

    public function getAllEmp() {
        $sql = "select id, nom, prenom from employe where idposte < 3 and etat = 1 order by nom";
        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);

        $res = $stmt->executeQuery();
        return $res->fetchAllAssociative();
    }

    public function getAllEmploye($id) {
        $valeur = array();

        $sql = "select id, nom, prenom from employe where id != :idempl order by nom";
        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);

        $res = $stmt->executeQuery(array("idempl" => $id));
        $result = $res->fetchAllAssociative();

        for($i=0; $i<count($result); $i++) {
            $rs = array("label" => $result[$i]['nom']." ".$result[$i]['prenom'], "id" => $result[$i]['id']);
            array_push($valeur, $rs);
        }

        return $valeur;
    }

    public function getAllUtilisateur($idemploye) {
        $sql = "";
        $conn = $this->getEntityManager()->getConnection();
        $result = array();

        if($idemploye == 0) {
            $sql = "select id, nom, prenom, email from employe where etat = 1 order by nom";
            $stmt = $conn->prepare($sql);
            $res = $stmt->executeQuery();
            $result = $res->fetchAllAssociative();
        }
        else {
            $sql = "select id, nom, prenom, email from employe where id = :idemp and etat = 1";
            $stmt = $conn->prepare($sql);
            $res = $stmt->executeQuery(array("idemp" => $idemploye));
            $result = $res->fetchAllAssociative();
        }

        return $result;
    }

    public function getRechercheEmp() {
        $valeur = array();

        $t = array("label" => "Tout", "id" => 0);
        array_push($valeur, $t);

        $sql = "select id, nom, prenom from employe order by nom";
        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);

        $res = $stmt->executeQuery();
        $result = $res->fetchAllAssociative();

        for($i=0; $i<count($result); $i++) {
            $rs = array("label" => $result[$i]['nom']." ".$result[$i]['prenom'], "id" => $result[$i]['id']);
            array_push($valeur, $rs);
        }
        return $valeur;
    }

    public function infoperso($id) {
        $sql = "select e.nom, e.prenom, e.email, e.adresse, e.telephone, e.skype, v.nom ville, p.nom poste from employe e join ville v on v.id = e.idville join poste p on p.id = e.idposte where e.id = :idemp";

        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);

        $res = $stmt->executeQuery(array("idemp" => $id));
        $result = $res->fetchAssociative();

        return $result;
    }

    public function getSameEmail($email, $idemp) {
        $sql = "select count(id) nombre from employe where email = :email and etat = 1 and id != :idemp";

        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $res = $stmt->executeQuery(array("email" => $email, "idemp" => $idemp));
        $result = $res->fetchAssociative();

        if($result['nombre'] > 0) {
            return true;
        }

        return false;
    }

    public function ficheEmp($id) {
        $sql = "select nom, prenom, solde_conge from employe where id = :idemp";
        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $res = $stmt->executeQuery(array("idemp" => $id));
        $result = $res->fetchAssociative();

        return $result;
    }
}
