<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\NatureRepository;

class NatureController extends AbstractController
{

    private $naturerepo;

    public function __construct(NatureRepository $nature)
    {
        $this->naturerepo = $nature;
    }

    /**
     * @Route("/api/nature", name="app_nature")
     */
    public function index(): Response
    {
        return $this->json($this->naturerepo->findAll());
    }
}
