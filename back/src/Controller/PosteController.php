<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PosteService;

class PosteController extends AbstractController
{

    private $posteservice;

    public function __construct(PosteService $poste)
    {
        $this->posteservice = $poste;
    }

    /**
     * @Route("/api/postes", name="app_poste")
     */
    public function index(): Response
    {
        $postes = $this->posteservice->findPoste();
        return $this->json($postes);
    }
}
