<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\AbsenceService;
use App\Entity\Absence;
use Symfony\Component\HttpFoundation\Request;

class AbsenceController extends AbstractController
{
    private $absservice;

    public function __construct(AbsenceService $abs)
    {
        $this->absservice = $abs;
    }

    /**
     * @Route("/api/absence", name="app_absence", methods={"POST"})
     */
    public function index(Request $request): Response
    {
        $content = $request->getContent();
        $value = json_decode($content);
        
        $absence = new Absence();
        $dateAbsence = new \DateTime($value->dateabsence);
        $absence->setDateabsence($dateAbsence);
        $absence->setIdemploye($value->idemploye);
        $absence->setCreatedat(new \DateTime());
        $enregistrement = $this->absservice->enregisterAbs($absence);

        return $this->json($enregistrement);
    }

    /**
     * @Route("/api/historique/absence/{numero}/{idemp}", name="app_historique_absence",methods={"GET"})
     */
    public function historiqueAbs($numero,$idemp): Response  
    {
        $listeAbs = $this->absservice->getHistoriqueAbs($numero, $idemp);

        return $this->json($listeAbs);
    }

    /**
     * @Route("/api/stat/absence", name="app_stat_absence",methods={"GET"})
     */
    public function statAbs(): Response  
    {
        $listeabs = $this->absservice->statAbsence();

        return $this->json($listeabs);
    }
}
