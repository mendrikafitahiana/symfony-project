<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\VilleService;

class VilleController extends AbstractController
{
    private $villeservice;

    public function __construct(VilleService $ville)
    {
        $this->villeservice = $ville;
    }
    
    /**
     * @Route("/api/villes", name="app_ville")
     */
    public function index(): Response
    {
        $villes = $this->villeservice->findVille();
        return $this->json($villes);
    }
}
