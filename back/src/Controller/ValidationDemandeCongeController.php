<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\ValidationDemandeCongeService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class ValidationDemandeCongeController extends AbstractController
{
    private $validationdemandeservice;

    public function __construct(ValidationDemandeCongeService $vd) 
    {
        $this->validationdemandeservice = $vd;
    }

    /**
     * @Route("/api/validation/demande/conge/{idempvalidateur}", name="app_validation_demande_conge",methods={"GET"})
     */
    public function index($idempvalidateur): JsonResponse
    {
       $validation = $this->validationdemandeservice->getDemandeByEmpValidateur($idempvalidateur);

       return $this->json($validation);
    }

    /**
     * @Route("/api/envoi/validation/demande/conge", name="app_envoi_validation_demande_conge",methods={"PUT"})
     */
    public function validerdemande(Request $request): Response {

        $content = $request->getContent();
        $value = json_decode($content);

        try{
            $this->validationdemandeservice->validationDemandeConge($value->etat, $value->iddemande, $value->idemployevalidateur, $value->nomemployevalidateur, $value->prenomvalidateur, $value->datedebut, $value->datefin, $value->emailempdemandeur, $value->emailempv, $value->idempd, $value->nombrejours, $value->comsvalidation);
            return new Response("Succès");  
            // return $this->json( $this->validationdemandeservice->validationDemandeConge($value->etat, $value->iddemande, $value->idemployevalidateur, $value->nomemployevalidateur, $value->prenomvalidateur, $value->datedebut, $value->datefin, $value->emailempdemandeur, $value->emailempv, $value->idempd, $value->nombrejours));         
        }
        catch(Exception $e) {
            // return new Response("Erreur : "+$e->getMessage());
        }

    //    return $this->json($value);

    }

     /**
     * @Route("/api/annulation/demande/conge", name="app_annuler_demande_conge",methods={"PUT"})
     */
    public function annulerdemande(Request $request): Response {

        $content = $request->getContent();
        $value = json_decode($content);

        $result = $this->validationdemandeservice->annulerDemande($value->iddemande);

       return $this->json($result);

    }
}
