<?php

namespace App\Controller;


use App\Entity\DemandeConge;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\DemandeCongeService;
use Symfony\Component\HttpFoundation\Request;

class DemandeCongeController extends AbstractController
{
    private $demandecongeservice;

    public function __construct(DemandeCongeService $demande)
    {
        $this->demandecongeservice = $demande;
    }


    /**
     * @Route("/api/demande/conge", name="app_demande_conge",methods={"POST"})
     */
    public function demande(Request $request): Response
    {
        $content = $request->getContent();
        $value = json_decode($content);

        $demande = new DemandeConge();
        $dateDeb = new \DateTime($value->datedebut);
        $demande->setDateDebut($dateDeb);
        $dateFin = new \DateTime($value->datefin);
        $demande->setDateFin($dateFin);
        $demande->setIdnatureConge($value->idnature);
        $demande->setCommentaires($value->commentaires);
        $demande->setIdemploye($value->idemploye);
        // $demande->setEtat($value->etat);
        $demande->setNombrejours($value->nombrejours);

        $bouton = $value->bouton;

        if($bouton == "envoyer") {
            $demande->setEtat(2);
        }
        else {
            $demande->setEtat(1);
        }
        $empvalidateur = $value->idemployevalidateur;

        $valiny = $this->demandecongeservice->demandeConge($demande, $bouton, $empvalidateur);

        return $this->json($valiny);
    }

    /**
     * @Route("/test/{datedebut}/{datefin}/{emp}", name="app_test",methods={"GET"})
     */
    public function test($datedebut, $datefin, $emp): Response
    {
        // $dateDeb = new \DateTime($datedebut);
        // $dateFin = new \DateTime($datefin);

        $valiny = $this->demandecongeservice->getNb($datedebut, $datefin, $emp);

        return new Response($valiny);
    }

    /**
     * @Route("/api/envoi/enregistrement", name="app_envoi_enregistrement",methods={"PUT"})
     */
    public function envoiApresEnregistrement(Request $request): Response
    {
        $content = $request->getContent();
        $value = json_decode($content);

        $valiny = $this->demandecongeservice->envoiApresEnregistrement($value->iddemande);

        return new Response($valiny);
    }

    /**
     * @Route("/api/modif/demande/{iddemande}", name="app_modif_demande",methods={"GET"})
     */
    public function modif($iddemande): Response
    {   
        // $content = $request->getContent();
        // $value = json_decode($content);

        $valiny = $this->demandecongeservice->findCongeById($iddemande);

        return $this->json($valiny);
    }

    /**
     * @Route("/api/modifier/demande/conge", name="app_modifier_demande_conge",methods={"PUT"})
     */
    public function modifierDemande(Request $request): Response
    {   
        $content = $request->getContent();
        $value = json_decode($content);

        $valiny = $this->demandecongeservice->ModifierDemandeConge($value);

        return $this->json($valiny);
    }
    
    /**
     * @Route("/api/test/exceptions/conge/{idnature}/{idemp}", name="app_test_exceptions_conge",methods={"GET"})
     */
    public function exceptions($idnature, $idemp): Response
    {   
        $valiny = $this->demandecongeservice->exceptionConge($idnature, $idemp);

        return $this->json($valiny);
    }
}
