<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\ListeCongeService;
use Symfony\Component\HttpFoundation\Request;

class ListeCongeController extends AbstractController
{
    private $listecongeservice;

    public function __construct(ListeCongeService $liste) 
    {
        $this->listecongeservice = $liste;
    }


    /**
     * @Route("/api/liste/conge/global/{emp}/{statut}/{numero}", name="app_liste_conge",methods={"GET"})
     */
    public function index($emp, $statut, $numero): Response
    {
        $listeconge = $this->listecongeservice->getListeCongeGlobal($emp, $statut, $numero);

        return $this->json($listeconge);
    }

    /**
     * @Route("/api/detail/conge/{idconge}", name="app_detail_conge",methods={"GET"})
     */
    public function getDetailConge($idconge): Response  
    {
        $listeconge = $this->listecongeservice->getDetailConge($idconge);

        return $this->json($listeconge);
    }

    /**
     * @Route("/api/historique/conge/{numero}/{idemp}", name="app_historique_conge",methods={"GET"})
     */
    public function historiqueConge($numero,$idemp): Response  
    {
        $listeconge = $this->listecongeservice->getHistoriqueConge($numero, $idemp);

        return $this->json($listeconge);
    }

    /**
     * @Route("/api/calendrier/conge/{idemp}", name="app_calendrier_conge",methods={"GET"})
     */
    public function calendrierConge($idemp): Response  
    {
        $listeconge = $this->listecongeservice->getCalendrierConge($idemp);

        return $this->json($listeconge);
    }

    /**
     * @Route("/api/calendrier/tout/{idemp}", name="app_calendrier_tout",methods={"GET"})
     */
    public function calendrier($idemp): Response  
    {
        $listeconge = $this->listecongeservice->getToutCalendrier($idemp);

        return $this->json($listeconge);
    }

    /**
     * @Route("/api/tableau/bord/mois", name="app_tableau_bord",methods={"GET"})
     */
    public function tableaudebord(): Response  
    {
        $listeconge = $this->listecongeservice->tableauDeBord();

        return $this->json($listeconge);
    }


    /**
     * @Route("/api/now", name="app_get_now",methods={"GET"})
     */
    public function getNow(): Response  
    {
        $listeconge = $this->listecongeservice->getNow();

        return $this->json($listeconge);
    }

    /**
     * @Route("/api/stat/conge", name="app_statistique",methods={"GET"})
     */
    public function statConge(): Response  
    {
        $listeconge = $this->listecongeservice->statConge();

        return $this->json($listeconge);
    }
}
