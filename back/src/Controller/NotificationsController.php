<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\NotificationsService;

class NotificationsController extends AbstractController
{
    private $notifservice;

    public function __construct(NotificationsService $notif) {
        $this->notifservice = $notif;
    }


    /**
     * @Route("/api/nombre/notif/{idempc}", name="app_notifications", methods={"GET"})
     */
    public function index($idempc): Response
    {
        $nbnotif = $this->notifservice->nombreNotif($idempc);

        return $this->json($nbnotif);
    }

    /**
     * @Route("/api/liste/notif/{idempc}", name="app_liste_notifications", methods={"GET"})
     */
    public function getNotif($idempc) {
        $liste = $this->notifservice->getNotifs($idempc);

        return $this->json($liste);
    }

    /**
     * @Route("/api/voir/notif/{idnotif}", name="app_voir_notifications", methods={"PUT"})
     */
    public function setVNotif($idnotif) {
        try {
            $this->notifservice->setVuNotif($idnotif);
            return new Response("Succès");  
        }
        catch(Exception $e) {
            return $e->getMessage();
        }
    }
}
