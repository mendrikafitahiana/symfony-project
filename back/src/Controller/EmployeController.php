<?php

namespace App\Controller;

use App\Entity\Employe;
use App\Service\EmployeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\Request;
use  Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordDecoderInterface;
use Doctrine\Persistence\ManagerRegistry;

class EmployeController extends AbstractController
{
    private $tokenst;
    private $empserv;
    private $manager;

     /**
     * @param Security
     */
    private $security;

    public function __construct(TokenStorageInterface $token, Security $sec, EmployeService $emp, ManagerRegistry $man)
    {
        $this->tokenst = $token;
        $this->empserv = $emp;
        $this->manager = $man;
    }

    /**
     * @Route("/api/currentemploye", name="app_employe",methods={"GET"})
     */
    public function index(): Response
    {
        $user = $this->getUser();

        return $this->json($user);
    }

    /**
     * @Route("/api/employevalidateur", name="app_employe_validateur",methods={"GET"})
     */
    public function employevalidateur(): Response
    {
        $valiny = $this->empserv->getEmployeValidateur();

        return $this->json($valiny);
    }

    /**
     * @Route("/api/all/employe", name="app_emp_abs",methods={"GET"})
     */
    public function employemetyabsent(): Response
    {
        $valiny = $this->empserv->getAllEmp();

        return $this->json($valiny);
    }

    /**
     * @Route("/api/tout/employe/{idemp}", name="app_emp_cal",methods={"GET"})
     */
    public function getTEmploye($idemp) {
        $result = $this->empserv->getAllEmploye($idemp);

        return $this->json($result);
    }

    /**
     * @Route("/api/tout/utilisateur/{idemp}", name="app_all_utilisateur",methods={"GET"})
     */
    public function getAllUtilisateur($idemp) {
        $result = $this->empserv->getAllUtilisateur($idemp);

        return $this->json($result);
    }
    
    /**
     * @Route("/api/recherche/employe", name="app_recherche_utilisateur",methods={"GET"})
     */
    public function getRechercheUtilisateur() {
        $result = $this->empserv->getRechercheEmp();

        return $this->json($result);
    }


    /**
     * @Route("/api/inscription/user", name="app_inscription_user",methods={"POST"})
     */
    public function inscription(Request $request) {

        $content = $request->getContent();
        $value = json_decode($content);

        $result = $this->empserv->inscriptionUtilisateur($value);

        return $this->json($result);
    }

     /**
     * @Route("/api/info/perso/{id}", name="app_info_perso",methods={"GET"})
     */
    public function infoPerso($id) {

        $result = $this->empserv->infoperso($id);

        return $this->json($result);
    }

    /**
     * @Route("/api/modif/info/user", name="app_modif_info_user",methods={"PUT"})
     */
    public function modifInfoPerso(Request $request) {

        $content = $request->getContent();
        $value = json_decode($content);

        $result = $this->empserv->modifierInfo($value);

        return $this->json($result);
    }

    /**
     * @Route("/api/fiche/emp/{idemp}", name="app_fiche_user",methods={"GET"})
     */
    public function fiche($idemp) {

        $result = $this->empserv->ficheEmp($idemp);

        return $this->json($result);
    }

    /**
     * @Route("/api/modif/exceptions", name="app_modif_exceptions",methods={"PUT"})
     */
    public function modifexceptions(Request $request) {

        $content = $request->getContent();
        $value = json_decode($content);

        $result = $this->empserv->parametres($value);

        return $this->json($result);
    }

    /**
     * @Route("/api/update/solde", name="app_udpate_solde",methods={"PUT"})
     */
    public function updatesolde() {

        $result = $this->empserv->updateSoldeConge();

        return $this->json($result);
    }

    /**
     * @Route("/api/suppresion/user/{id}", name="app_suppression_user",methods={"PUT"})
     */
    public function supprimerUser($id) {

        $result = $this->empserv->supprimerUser($id);

        return $this->json($result);
    }


    /**
     * @Route("/api/verif/etat", name="app_verif_etat",methods={"GET"})
     */
    public function verifEtat() {

        $user = $this->getUser();
        $etat = $user->getEtat();
        $result = true;

        if($etat == 1 or $etat == null) {
            $result = true;
        }
        else {
            $result = false;
        }

        return $this->json($result);
    }
    
}
