-- ALTER TABLE ville
-- ADD CONSTRAINT FK_VillePays
-- FOREIGN KEY (idpays) REFERENCES pays(id);

-- ALTER TABLE employe
-- ADD CONSTRAINT FK_EmployeVille
-- FOREIGN KEY (idville) REFERENCES ville(id);

-- ALTER TABLE employe
-- ADD CONSTRAINT FK_EmployePoste
-- FOREIGN KEY (idposte) REFERENCES poste(id);

-- ALTER TABLE demandeconge
-- ADD CONSTRAINT FK_NatureConge
-- FOREIGN KEY (idnatureconge) REFERENCES nature(id);

-- ALTER TABLE demandeconge
-- ADD CONSTRAINT FK_EmployeDemande
-- FOREIGN KEY (idemploye) REFERENCES employe(id);

-- ALTER TABLE validation_demande_user
-- ADD CONSTRAINT FK_Demande
-- FOREIGN KEY (iddemandeconge) REFERENCES demandeconge(id);

-- ALTER TABLE validation_demande_user
-- ADD CONSTRAINT FK_EmployeValidateur
-- FOREIGN KEY (idemployevalidateur) REFERENCES employe(id);

-- Liste conge globale par statut
select * from listeconge where idposte = 3 and idemploye = 1; 

-- Historique de congés 

-- 7jours
select * from listeconge where date(createdat) >= (now() - interval 7 day)

-- 3mois
select * from listeconge where date(createdat) >= (now() - interval 3 month)

-- 6mois
select * from listeconge where date(createdat) >= (now() - interval 6 month)

-- 1an
select * from listeconge where date(createdat) >= (now() - interval 1 year)

-- Calendrier
select date_debut, date_fin from listeconge where etat_validation = 3 and idemploye = ? and idposte = 3

-- table notifications : (id, vu_le date nullable, vue boolean, createur int, concerner int, notif text, dateDebut date, dateFin date, nature string)

-- nb notifs
select count(id) nbnotif from notifications where concerner = 2 and vue = false

-- insertion exceptions
insert into exceptions values(null, 1, 15, 1),
(null, 2, 7, 1),
(null, 1, 15, 2), 
(null, 2, 7, 2) 

-- evenements pour updater solde conge chaque mois (mysql event scheduler)
-- create event if not exists solde_conge_mois
-- on schedule at current_date + interval 1 month
-- on completion preserve 
-- do 
--     update employe e inner join employe emp on e.id = emp.id 
--     set e.solde_conge = emp.solde_conge+2.5

    -- TABLEAU DE BORD CONGE FINAL
select employe.id, employe.nom, employe.prenom, employe.solde_conge, employe.solde_conge + 2.5 solde_conge_prochain, 
(case when tb.totalconge is null then 0.00 else tb.totalconge end) totalconge
    from tb 
    right join employe on employe.id = tb.id

-- TABLEAU DE BORD ABSENCE FINAL
select employe.id, employe.nom, employe.prenom, employe.solde_conge, employe.solde_conge + 2.5 solde_conge_prochain, 
(case when tba.totalabsence is null then 0.00 else tba.totalabsence end) totalabsence
    from tba 
    right join employe on employe.id = tba.id

    -- nombre de personnes congés chaque mois 

select month(date_debut) mois, count(id) nbconge from listeconge where etat_validation = 3 and idposte = 3 and year(date_debut) = year(current_date)
    group by month(date_debut)

-- Statistiques du nombre de personnes congés chaque mois dans une année
select 
	(case  
		when m.mois = 1 then 'Janvier'
		when m.mois = 2 then 'Février'
		when m.mois = 3 then 'Mars'
		when m.mois = 4 then 'Avril'
		when m.mois = 5 then 'Mai'
		when m.mois = 6 then 'Juin'
		when m.mois = 7 then 'Juillet'
		when m.mois = 8 then 'Août'
		when m.mois = 9 then 'Septembre'
		when m.mois = 10 then 'Octobre'
		when m.mois = 11 then 'Novembre'
		else 'Décembre'
	end) mois, 
	(case when s.nbconge is null then 0 else s.nbconge end) nbconge
    from
    (
        SELECT 1 AS mois
        UNION SELECT 2 AS mois
        UNION SELECT 3 AS mois
        UNION SELECT 4 AS mois
        UNION SELECT 5 AS mois
        UNION SELECT 6 AS mois
        UNION SELECT 7 AS mois
        UNION SELECT 8 AS mois
        UNION SELECT 9 AS mois
        UNION SELECT 10 AS mois
        UNION SELECT 11 AS mois
        UNION SELECT 12 AS mois
    ) as m
left join stat s on s.mois = m.mois
group by m.mois 


-- Statistiques du nombre de personnes absents chaque mois

select month(dateabsence) mois, count(id) nbconge from absence
    where year(dateabsence) = year(current_date)
    group by month(dateabsence)


-- Statistiques du nombre de personnes absents chaque mois dans une année

select 
    (case
        when m.mois = 1 then 'Janvier'
		when m.mois = 2 then 'Février'
		when m.mois = 3 then 'Mars'
		when m.mois = 4 then 'Avril'
		when m.mois = 5 then 'Mai'
		when m.mois = 6 then 'Juin'
		when m.mois = 7 then 'Juillet'
		when m.mois = 8 then 'Août'
		when m.mois = 9 then 'Septembre'
		when m.mois = 10 then 'Octobre'
		when m.mois = 11 then 'Novembre'
		else 'Décembre'
	end) mois,
    (case when s.nbabsents is null then 0 else s.nbabsents end) nbabsents
    from 
    (
        SELECT 1 AS mois
        UNION SELECT 2 AS mois
        UNION SELECT 3 AS mois
        UNION SELECT 4 AS mois
        UNION SELECT 5 AS mois
        UNION SELECT 6 AS mois
        UNION SELECT 7 AS mois
        UNION SELECT 8 AS mois
        UNION SELECT 9 AS mois
        UNION SELECT 10 AS mois
        UNION SELECT 11 AS mois
        UNION SELECT 12 AS mois
    ) as m
    left join statabs s on s.mois = m.mois
    group by m.mois 

    204e212f4a15cca573dd520769523377-07e2c238-cee9f39e