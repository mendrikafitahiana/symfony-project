-------------------------------- PART 1 ------------------------------------

CREATE DATABASE `gesconge`
	DEFAULT CHARACTER SET `utf8`
	DEFAULT COLLATE       `utf8_general_ci`;
 

USE gesconge;

------------------------ END PART 1 ----------------------------------------



------------------------ PART 2 -------------------------------------------

create table absence(
    id int not null AUTO_INCREMENT PRIMARY KEY,
    dateabsence date not null,
    idemploye int not null
);

------------------------ END PART 2 ---------------------------------------



------------------------ PART 3 -------------------------------------------

insert into pays (nom) values ('Madagascar');

insert into ville (nom,idpays) values ('Antananarivo',1);

insert into poste(nom) values ('Développeur'),
('Chef de projet'),
('Administrateur');


insert into nature(nom) values ('Congé annuel'),
('Congé de récupération'),
('Congé maladie');


create or replace view demandeetvalidation as
    select d.id, d.date_debut, d.date_fin, d.idnature_conge,
    d.commentaires, d.idemploye, d.etat etat_demande, d.nombrejours,
    v.etat etat_validation
        from demande_conge d
        left join validation_demande_user v on v.iddemandeconge = d.id;

alter table demande_conge add column createdat timestamp default current_timestamp;

alter table notifications add column createdat timestamp default current_timestamp;

alter table employe add column etat int default 1;

-- TABLEAU DE BORD CONGE1
create or replace view tb as
select employe.id, sum(dc.nombrejours) totalconge
    from employe join demande_conge dc on employe.id = dc.idemploye
    join validation_demande_user vd on dc.id = vd.iddemandeconge
    join employe ev on ev.id = vd.idemployevalidateur
    where vd.etat = 3 and ev.idposte = 3 
    and month(dc.date_debut) = month(current_date) and month(dc.date_fin) = month(current_date)
    and year(dc.date_debut) = year(current_date) and year(dc.date_fin) = year(current_date)
    group by employe.id;


-- TABLEAU DE BORD ABSENCE1
create or replace view tba as
select employe.id, count(a.dateabsence) totalabsence
    from employe join absence a on a.idemploye = employe.id 
    where month(a.dateabsence) = month(current_date) and year(a.dateabsence) = year(current_date)
    group by employe.id;


-- pour update soldeconge
-- update employe set date_update = CURRENT_DATE;

create or replace view listeconge as
    select dv.*, n.nom natureconge, e.id idemployevalidateur, e.idposte, e.nom nomvalidateur, 
    e.prenom prenomvalidateur, ed.nom nomdemandeur, ed.prenom prenomdemandeur, ed.email,e.email emailempv, ev.etat etat_validation, ev.commentaires_validation, 
    (case 
        when ev.etat = 1 then "Enregistrée"
        when ev.etat = 2 then "Envoyée"
        when ev.etat = 3 then "Validée"
        when ev.etat = 4 then "Refusée"
        when ev.etat = -1 then "Annulée"
        when ev.etat = 5 then "En réserve"
    end) statut,
    (case   
        when e.idposte = 1 then "Développeur"
        when e.idposte = 2 then "Chef de projet"
        when e.idposte = 3 then "Gestionnaire(Administateur)"
    end) poste
        from demande_conge dv 
        join validation_demande_user ev on ev.iddemandeconge = dv.id
        join employe e on ev.idemployevalidateur = e.id
        join employe ed on ed.id = dv.idemploye
        join nature n on n.id = dv.idnature_conge;

-- STAT CONGE
create or replace view stat as
select month(date_debut) mois, count(id) nbconge from listeconge where etat_validation = 3 and idposte = 3 and year(date_debut) = year(current_date)
    group by month(date_debut);

update absence set createdat = current_timestamp;

create or replace view listeabsence as
select e.id, e.nom nomabsent, e.prenom prenomabsent, a.dateabsence, a.createdat 
    from employe e join absence a on a.idemploye = e.id;

-- STAT ABSENCE
create or replace view statabs as
select month(dateabsence) mois, count(id) nbabsents from absence
    where year(dateabsence) = year(current_date)
    group by month(dateabsence);





---------------------------- END PART 3 ---------------------------------------------













